drop schema if exists chess;
create schema chess;
use chess;

create table player (
    player_id int not null auto_increment primary key,
    username varchar(30) not null ,
    password char(32) not null ,
    constraint uq_username unique (username)
);

create table game(
    game_id int not null auto_increment primary key,
	white_player_id int not null ,
    black_player_id int not null ,
    winner_id int,
    status enum('WON','IN_PROGRESS','DRAW'),
    game_time bigint,
    foreign key game_white_player_fk (white_player_id) references player(player_id),
    foreign key game_black_player_fk (black_player_id) references player(player_id),
    foreign key game_winner_player_fk (winner_id) references player(player_id)
);

create table piece (
    piece_code char(2) not null primary key,
    color enum('WHITE','BLACK') not null,
    type enum ('ROOK','PAWN','KNIGHT','KING','BISHOP','QUEEN')
);


create table action (
    action_id int not null auto_increment primary key ,
    game_id int not null,
    player_id int not null,
    name varchar(10) not null,
    foreign key action_game_fk (game_id) references game(game_id),
    foreign key action_player_fk (player_id) references player(player_id)
);

create table move(
    move_id int not null auto_increment primary key ,
    action_id int not null,
    piece_code char(2) not null,
    `from` char(2),
    `to` char(2),
    foreign key move_action_fk (action_id) references action(action_id),
    foreign key move_piece_fk (piece_code) references piece(piece_code)
);
