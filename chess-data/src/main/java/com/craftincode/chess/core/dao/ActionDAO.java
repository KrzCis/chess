package com.craftincode.chess.core.dao;

import com.craftincode.chess.core.dto.ActionDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ActionDAO {
    private Connection conn;

    public ActionDAO(Connection conn) {
        this.conn = conn;
    }

    public List<ActionDTO> findAll() throws SQLException {
        String sql = "select action_id, game_id, player_id, name from action";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();

        List<ActionDTO> actionDTOS = new ArrayList<>();

        Integer actionId, gameId, playerId;
        String name;

        while (rs.next()) {
            actionId = rs.getInt("action_id");
            gameId = rs.getInt("game_id");
            playerId = rs.getInt("player_id");
            name = rs.getString("name");
            actionDTOS.add(new ActionDTO(actionId, gameId, playerId, name));
        }
        return actionDTOS;
    }

    public ActionDTO findById(int id) throws SQLException {
        String sql = "select action_id, game_id, player_id, name from action where action_id = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();

        Integer actionId, gameId, playerId;
        String name;

        if (rs.next()) {
            actionId = rs.getInt("action_id");
            gameId = rs.getInt("game_id");
            playerId = rs.getInt("player_id");
            name = rs.getString("name");
            return new ActionDTO(actionId, gameId, playerId, name);
        }

        return null;
    }

    public ActionDTO create(ActionDTO actionDTO) throws SQLException {
        String sql = "insert into action (game_id, player_id, name) values (?, ?, ?)";
        PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ps.setInt(1, actionDTO.getGameId());
        ps.setInt(2, actionDTO.getPlayerId());
        ps.setString(3, actionDTO.getName());
        ps.executeUpdate();

        ResultSet rs = ps.getGeneratedKeys();

        if (rs.next()) {
            return new ActionDTO(rs.getInt(1), actionDTO.getGameId(), actionDTO.getPlayerId(),
                    actionDTO.getName());
        }
        return null;
    }

    public void update(ActionDTO actionDTO) throws SQLException {
        String sql = "update action set game_id = ?, player_id = ?, name = ? where action_id = ?";
        PreparedStatement ps = conn.prepareStatement(sql);

        ps.setInt(1, actionDTO.getGameId());
        ps.setInt(2, actionDTO.getPlayerId());
        ps.setString(3, actionDTO.getName());
        ps.setInt(4, actionDTO.getActionId());
        ps.executeUpdate();
    }

    public void delete(ActionDTO actionDTO) throws SQLException {
        String sql = "delete from action where action_id = ?";

        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, actionDTO.getActionId());
        ps.executeUpdate();
    }
}
