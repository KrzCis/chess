package com.craftincode.chess.core.dto;

public class MoveDTO {
    private Integer moveId;
    private  Integer actionId;
    private String pieceCode;
    private String from;
    private String to;

    public MoveDTO(Integer moveId, Integer actionId, String pieceCode, String from, String to) {
        this.moveId = moveId;
        this.actionId = actionId;
        this.pieceCode = pieceCode;
        this.from = from;
        this.to = to;
    }

    public Integer getMoveId() {
        return moveId;
    }

    public void setMoveId(Integer moveId) {
        this.moveId = moveId;
    }

    public Integer getActionId() {
        return actionId;
    }

    public void setActionId(Integer actionId) {
        this.actionId = actionId;
    }

    public String getPieceCode() {
        return pieceCode;
    }

    public void setPieceCode(String pieceCode) {
        this.pieceCode = pieceCode;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
