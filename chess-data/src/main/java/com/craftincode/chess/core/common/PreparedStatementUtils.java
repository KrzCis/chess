package com.craftincode.chess.core.common;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Objects;

public class PreparedStatementUtils {

    public static void setNullableParam(PreparedStatement ps, int paramIndex, Object value, int type) throws SQLException {
        if(Objects.isNull(value)) {
            ps.setNull(paramIndex,type);
        } else {
            ps.setObject(paramIndex,value,type);
        }
    }
}
