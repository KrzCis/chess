package com.craftincode.chess.core.service;

import com.craftincode.chess.core.common.ConnectionUtils;
import com.craftincode.chess.core.dao.GameDAO;
import com.craftincode.chess.core.dao.PlayerDAO;
import com.craftincode.chess.core.dto.GameDTO;
import com.craftincode.chess.core.dto.PlayerDTO;
import com.craftincode.chess.core.model.Game;
import com.craftincode.chess.core.model.GameStatus;
import com.craftincode.chess.core.model.Player;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


public class GameService {
    private Connection conn;
    private GameDAO gameDAO;
    private PlayerDAO playerDAO;

    public GameService() throws SQLException {
        conn = ConnectionUtils.getConnection();
        gameDAO = new GameDAO(conn);
        playerDAO = new PlayerDAO(conn);
    }

    public List<Game> findAll() throws Exception {
        List<GameDTO> gameDTOs = gameDAO.findAll();
        List<Game> games = new ArrayList<>();

        for (GameDTO gameDTO : gameDTOs) {
            Game game = assembleGame(gameDTO, playerDAO);
            games.add(game);
        }
        return games;
    }


    public Optional<Game> findById(int id) throws Exception {
        GameDTO gameDTO = gameDAO.findById(id);
        if (Objects.isNull(gameDTO)) return Optional.empty();
        return Optional.of(assembleGame(gameDTO, playerDAO));
    }

    public Game save(Game game) throws Exception {
        GameDTO gameDTO = new GameDTO(game.gameId,
                game.whitePlayer.playerId,
                game.blackPlayer.playerId,
                game.winner.playerId,
                game.gameTime,
                game.status.toString()
        );
        if (Objects.isNull(game.gameId)) {
            gameDTO = gameDAO.create(gameDTO);
            return new Game(gameDTO.getGameId(), game.whitePlayer, game.blackPlayer, game.winner, game.status, game.gameTime);
        } else {
            gameDAO.update(gameDTO);
            return game;
        }
    }

    private Game assembleGame(GameDTO gameDTO, PlayerDAO playerDAO) throws SQLException {
        Integer gameId = gameDTO.getGameId();
        PlayerDTO whiteDto = playerDAO.findById(gameDTO.getWhitePlayerId());
        PlayerDTO blackDto = playerDAO.findById(gameDTO.getBlackPlayerId());
        PlayerDTO winnerDto = playerDAO.findById(gameDTO.getWinnerId());

        Player white= new Player(whiteDto.getPlayerId(),whiteDto.getUsername(),whiteDto.getPassword());
        Player black= new Player(blackDto.getPlayerId(),blackDto.getUsername(),blackDto.getPassword());
        Player winner= new Player(winnerDto.getPlayerId(),winnerDto.getUsername(),winnerDto.getPassword());

        GameStatus status = GameStatus.valueOf(gameDTO.getStatus());
        Long gameTime = gameDTO.getGameTime();
        return new Game(gameId, white, black, winner, status, gameTime);
    }

}
