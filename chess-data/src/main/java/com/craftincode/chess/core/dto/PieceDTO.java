package com.craftincode.chess.core.dto;

import com.craftincode.chess.core.model.PieceColor;
import com.craftincode.chess.core.model.PieceType;

public class PieceDTO {
    private PieceType pieceType;
    private PieceColor pieceColor;
    private String pieceCode;

    public PieceDTO(PieceType pieceType, PieceColor pieceColor, String pieceCode) {
        this.pieceType = pieceType;
        this.pieceColor = pieceColor;
        this.pieceCode = pieceCode;
    }

    public PieceType getPieceType() {
        return pieceType;
    }

    public void setPieceType(PieceType pieceType) {
        this.pieceType = pieceType;
    }

    public PieceColor getPieceColor() {
        return pieceColor;
    }

    public void setPieceColor(PieceColor pieceColor) {
        this.pieceColor = pieceColor;
    }

    public String getPieceCode() {
        return pieceCode;
    }

    public void setPieceCode(String pieceCode) {
        this.pieceCode = pieceCode;
    }
}
