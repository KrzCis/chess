package com.craftincode.chess.core.dao;

import com.craftincode.chess.core.dto.GameDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.craftincode.chess.core.common.PreparedStatementUtils.setNullableParam;

public class GameDAO {
    private Connection conn;

    public GameDAO(Connection conn) {
        this.conn = conn;
    }

    public List<GameDTO> findAll() throws SQLException {
        String sql = " select " +
                "game_id, white_player_id, black_player_id, winner_id, status, game_time " +
                "from game";

        PreparedStatement findAllPS = conn.prepareStatement(sql);
        ResultSet resultSet = findAllPS.executeQuery();
        List<GameDTO> games = new ArrayList<>();

        while (resultSet.next()) {
            games.add(convertRowToGameDTO(resultSet));
        }

        return games;
    }


    public GameDTO findById(int id) throws SQLException {
        String sql = "select " +
                "game_id, white_player_id, black_player_id, winner_id, status, game_time " +
                "from game " +
                "where game_id = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, id);

        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            return convertRowToGameDTO(rs);
        }
        return null;
    }

    private GameDTO convertRowToGameDTO(ResultSet resultSet) throws SQLException {
        Integer gameId, whitePlayerId,blackPlayerId,winnerId;
        String status;
        Long gameTime;
        gameId = resultSet.getInt("game_id");
        whitePlayerId = resultSet.getInt("white_player_id");
        blackPlayerId = resultSet.getInt("black_player_id");
        winnerId = resultSet.getInt("winner_id");
        if(resultSet.wasNull()) winnerId = null;
        status = resultSet.getString("status");
        gameTime = resultSet.getLong("game_time");
        if(resultSet.wasNull()) gameTime = null;
        return new GameDTO(gameId,whitePlayerId,blackPlayerId,winnerId,gameTime,status);
    }

    public GameDTO create(GameDTO game) throws SQLException { // Michal i Karol (game_create_imie)

        String insertSql = "insert into game (white_player_id, black_player_id, winner_id, status, game_time)" +
                "values (?, ?, ?, ?, ?)";

        PreparedStatement ps = conn.prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);
        ps.setInt(1, game.getWhitePlayerId());
        ps.setInt(2, game.getBlackPlayerId());
        setNullableParam(ps,3,game.getWinnerId(),Types.INTEGER);
        ps.setString(4, game.getStatus());
        setNullableParam(ps,5,game.getGameTime(),Types.BIGINT);
        ps.executeUpdate();

        ResultSet rs = ps.getGeneratedKeys();

        if (rs.next()) {
            return new GameDTO(rs.getInt(1), game.getWhitePlayerId(),game.getBlackPlayerId(),game.getWinnerId(),game.getGameTime(),game.getStatus());
        }

        return null;
    }

    public void update(GameDTO game) throws SQLException { // Krzysiek i Radka (game_update_imie)
        String sql = "update game set white_player_id = ?, black_player_id =?, winner_id = ?,  status =?, game_time =? " +
                "where game_id = ?";
        PreparedStatement ps = conn.prepareStatement(sql);

        ps.setInt(1, game.getWhitePlayerId());
        ps.setInt(2, game.getBlackPlayerId());
        setNullableParam(ps,3,game.getWinnerId(),Types.INTEGER);
        ps.setString(4, game.getStatus());
        setNullableParam(ps,5,game.getGameTime(),Types.BIGINT);
        ps.setInt(6, game.getGameId());
        ps.executeUpdate();

    }

    public void delete(GameDTO game){
    }


}
