package com.craftincode.chess.core.dto;

public class ActionDTO {
    private Integer actionId, gameId, playerId;
    private String name;

    public ActionDTO(Integer actionId, Integer gameId, Integer playerId, String name) {
        this.actionId = actionId;
        this.gameId = gameId;
        this.playerId = playerId;
        this.name = name;
    }

    public Integer getActionId() {
        return actionId;
    }

    public void setActionId(Integer actionId) {
        this.actionId = actionId;
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
