package com.craftincode.chess.core.dto;

public class PlayerDTO {
    private Integer playerId;
    private String username;
    private String password;

    public PlayerDTO(Integer playerId, String username, String password) {
        this.playerId = playerId;
        this.username = username;
        this.password = password;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassoword(String password) {
        this.password = password;
    }
}
