package com.craftincode.chess.core.dao;

import com.craftincode.chess.core.dto.PieceDTO;
import com.craftincode.chess.core.model.PieceColor;
import com.craftincode.chess.core.model.Piece;
import com.craftincode.chess.core.model.PieceType;

import java.sql.*;

public class PieceDAO {
    private Connection conn;

    public PieceDAO(Connection conn) {
        this.conn = conn;
    }

    public Piece create(PieceDTO piece) throws SQLException {
        String insertSql = "insert into piece (piece_code,color,type)" +
                "values (?, ?, ?)";

        PreparedStatement ps = conn.prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, piece.getPieceCode() == null ? null : piece.getPieceCode());
        ps.setString(2, String.valueOf(piece.getPieceColor()));
        ps.setString(3, String.valueOf(piece.getPieceType()));
        ps.executeUpdate();

        return new Piece(piece.getPieceType(),piece.getPieceColor());

    }
    public Piece findByPieceCode(String pieceCode) throws SQLException {
        String findBy = "select * from piece where piece_code = ?";
        PreparedStatement selectStmt = conn.prepareStatement(findBy);
        selectStmt.setString(1,pieceCode);
        ResultSet rs = selectStmt.executeQuery();
        if(rs.first()) {
            PieceColor pieceColor = PieceColor.valueOf(rs.getString("color"));
            PieceType pieceType = PieceType.valueOf(rs.getString("type"));

            return new Piece(pieceType, pieceColor);
        }

        return  null;
    }



}
