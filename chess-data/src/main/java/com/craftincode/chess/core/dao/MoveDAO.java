package com.craftincode.chess.core.dao;

import com.craftincode.chess.core.dto.MoveDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MoveDAO {
    private Connection conn;

    public MoveDAO (Connection conn){
        this.conn = conn;
    }

    public List<MoveDTO> findAll() throws SQLException {
        String sql = "select move_id, action_id, piece_code, from, to from move";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();

        List<MoveDTO> moves = new ArrayList<>();
        while (rs.next()) {
            Integer moveId = rs.getInt("move_id");
            Integer actionId = rs.getInt("action_id");
            String pieceCode = rs.getString("piece_code");
            String from = rs.getString("from");
            String to = rs.getString("to");
            moves.add(new MoveDTO(moveId, actionId, pieceCode, from, to));
        }
        return moves;
    }

    public MoveDTO findById(int id) throws SQLException {
        String sql = "select action_id, piece_code, from, to from move where move_id = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, id);
        ResultSet resultSet = ps.executeQuery();
        Integer actionId;
        String pieceCode,from,to;
        if (resultSet.next()) {
            actionId = resultSet.getInt("action_id");
            pieceCode = resultSet.getString("piece_code");
            from = resultSet.getString("from");
            to = resultSet.getString("to");
        return new MoveDTO(id, actionId, pieceCode, from, to);
        }
        return null;
    }

    public MoveDTO create (MoveDTO move) throws SQLException {
        String sql = "insert into move (move_id, action_id, piece_code, from, to)" +
                "values (?, ?, ?, ?, ?)";
        PreparedStatement preparedStatement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, move.getMoveId());
        preparedStatement.setInt(2, move.getActionId());
        preparedStatement.setString(3, move.getPieceCode());
        preparedStatement.setString(4, move.getFrom());
        preparedStatement.setString(5, move.getTo());
        preparedStatement.executeUpdate();
        MoveDTO moveDTO = new MoveDTO(move.getMoveId(), move.getActionId(),
                move.getPieceCode(), move.getFrom(), move.getTo());
        ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
        if (generatedKeys.next()) {
            return new MoveDTO(move.getMoveId(), move.getActionId(),
                    move.getPieceCode(), move.getFrom(), move.getTo());
        }else {
            return null;
        }
    }

    public void update (MoveDTO move) throws SQLException {
        String sql = "update move set  action_id = ?, piece_code = ?, from = ?, to = ? where move_id =?";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setInt(1, move.getActionId());
        preparedStatement.setString(2, move.getPieceCode());
        preparedStatement.setString(3, move.getFrom());
        preparedStatement.setString(4, move.getTo());
        preparedStatement.setInt(5, move.getMoveId());
        preparedStatement.executeUpdate();
    }
}
