package com.craftincode.chess.core.dto;

public class GameDTO {
    private Integer gameId, whitePlayerId, blackPlayerId,winnerId;
    private Long gameTime;
    private String status;

    public GameDTO(Integer gameId, Integer whitePlayerId, Integer blackPlayerId, Integer winnerId, Long gameTime, String status) {
        this.gameId = gameId;
        this.whitePlayerId = whitePlayerId;
        this.blackPlayerId = blackPlayerId;
        this.winnerId = winnerId;
        this.gameTime = gameTime;
        this.status = status;
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public Integer getWhitePlayerId() {
        return whitePlayerId;
    }

    public void setWhitePlayerId(Integer whitePlayerId) {
        this.whitePlayerId = whitePlayerId;
    }

    public Integer getBlackPlayerId() {
        return blackPlayerId;
    }

    public void setBlackPlayerId(Integer blackPlayerId) {
        this.blackPlayerId = blackPlayerId;
    }

    public Integer getWinnerId() {
        return winnerId;
    }

    public void setWinnerId(Integer winnerId) {
        this.winnerId = winnerId;
    }

    public Long getGameTime() {
        return gameTime;
    }

    public void setGameTime(Long gameTime) {
        this.gameTime = gameTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
