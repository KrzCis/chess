package com.craftincode.chess.core.dao;

import com.craftincode.chess.core.dto.PlayerDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PlayerDAO {

    private Connection conn;
    public PlayerDAO(Connection conn)  {
        this.conn = conn;
    }

    public List<PlayerDTO> findAll() throws SQLException { // Krzysiek i Radka (player_find_all)
        String selectSql = "select player_id, username, password from player";
        PreparedStatement selectDB = conn.prepareStatement(selectSql);
        ResultSet rs=selectDB.executeQuery();
        List<PlayerDTO> players = new ArrayList<>();
        while (rs.next()){

            int playerId = rs.getInt("player_id");
            String username = rs.getString("username");
            String password = rs.getString("password");
            players.add(new PlayerDTO(playerId,username,password));
        }
    return players;
    }

    public PlayerDTO findById(int id) throws SQLException { //Michal i Karol (player_find_by_id)
        String findBy = "select * from player where player_id = ?";
        PreparedStatement selectStmt = conn.prepareStatement(findBy);
        selectStmt.setInt(1,id);
        ResultSet rs = selectStmt.executeQuery();
        if(rs.first()) {
            String username = rs.getString("username");
            String password = rs.getString("password");
            return new PlayerDTO(id, username, password);
        }

        return  null;
    }


    public void update(PlayerDTO player) throws SQLException { // Mateusz, Konrad, Michal (player_udpate)
        String updateSql = "update player set username = ?, password = ? where player_id = ?";
        PreparedStatement updatePS = conn.prepareStatement(updateSql);

        updatePS.setString(1, player.getUsername());
        updatePS.setString(2, player.getPassword());
        updatePS.setInt(3, player.getPlayerId());
        updatePS.executeUpdate();
    }

    public void delete(PlayerDTO player){

    }

    public PlayerDTO create(PlayerDTO player) throws SQLException {
        String insertSql =
                "insert into player(username,password) values (?,?)";
        PreparedStatement insertPS = conn.prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);
        insertPS.setString(1,player.getUsername());
        insertPS.setString(2,player.getPassword());
        insertPS.executeUpdate();
        ResultSet keysRs = insertPS.getGeneratedKeys();
        if(keysRs.first()){
            return new PlayerDTO(keysRs.getInt(1), player.getUsername(),player.getPassword());
        }
        throw new SQLException("Could not fetch autogenerated key");
    }
}
