import java.sql.*;

public class JdbcDemo {
    public static void main(String[] args) throws SQLException {
        String jdbcUrl = "jdbc:mysql://localhost:3306/chess";
        String dbUser = "root";
        String dbPassword = "root";
        Connection conn = DriverManager.getConnection(jdbcUrl, dbUser, dbPassword);

        // ##INSERT PLAYERA##
//        String insertSql =
//                "insert into player (username,password) values ('admin', md5('adminadmin'))";
//        PreparedStatement insertPS = conn.prepareStatement(insertSql);
//        insertPS.executeUpdate(insertSql);

        // ##SELECT Z TABELI PLAYER##
        String selectSql = "select player_id, username,password from player";
        PreparedStatement selectStmt = conn.prepareStatement(selectSql);
        ResultSet rs = selectStmt.executeQuery();
        while (rs.next()){ // dopoki są jeszcze jakieś wiersze
            int playerId = rs.getInt(1); // daj mi w danym wierszu, wartość z kolumny pierwszej (numerowane od 1)
            playerId = rs.getInt(
                    "player_id"); // albo przez nazwę kolumny
            String username = rs.getString("username");
            String password = rs.getString("password");
            System.out.printf("%d\t|%s\t|%s%n", playerId,username,password);
        }
    }
}
