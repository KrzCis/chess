package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.*;

public class PawnVectorMoveValidator implements MoveValidator {
    private Move move;
    private PieceColor pieceColor;

    public PawnVectorMoveValidator(Move move, PieceColor pieceColor) {
        this.move = move;
        this.pieceColor = pieceColor;
    }


    @Override
    public boolean isValid() {
        if (movedHorizontally(move)) {
            return false;
        }
        if (movedMoreThan2()) return false;
        if (movedInWrongDirection(pieceColor)) return false;
        if (moved2NotFromStartingPosition(pieceColor)) return false;

        return true;
    }

    private boolean movedHorizontally(Move move) {
        return move.horizontalShift() != 0;
    }

    private boolean moved2NotFromStartingPosition(PieceColor pieceColor) {
        int directionModifier = pieceColor.equals(PieceColor.WHITE) ? -1 : 1;
        int startingRow = pieceColor.equals(PieceColor.WHITE) ? 6 : 1;
        return move.verticalShift() == 2 * directionModifier && move.start.row != startingRow;
    }

    private boolean movedInWrongDirection(PieceColor pieceColor) {
        int directionModifier = pieceColor.equals(PieceColor.WHITE) ? -1 : 1;
        return move.verticalShift() * directionModifier < 0;
    }

    private boolean movedMoreThan2() {
        return Math.abs(move.verticalShift()) > 2;
    }
}
