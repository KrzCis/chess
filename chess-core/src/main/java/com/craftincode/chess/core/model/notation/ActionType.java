package com.craftincode.chess.core.model.notation;

public enum ActionType {
    CHECKMATE, CHECK, CAPTURE, ENPASSANT, PROMOTED, QUEENSIDECASTLING, KINGSIDECASTLING, MOVE
}
