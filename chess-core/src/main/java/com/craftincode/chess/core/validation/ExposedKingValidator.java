package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.*;
import com.craftincode.chess.core.model.notation.ActionType;
import com.craftincode.chess.core.model.notation.GameNotation;

import java.util.ArrayList;
import java.util.List;
//todo
public class ExposedKingValidator implements MoveValidator { //you can't move to allow check to your own king
    //you can move when last move wasn't your color

    private Board board;
    private Move move;
    private GameNotation gameNotation;


    public ExposedKingValidator(Board board, Move move, GameNotation gameNotation) {
        this.board = board;
        this.move = move;
        this.gameNotation = gameNotation;

    }

    @Override
    public boolean isValid() {
        if(whoAttack(board,gameNotation).size()>0) return move.getDirection()==direction(board);
        return true;
    }


    private MoveDirection direction(Board board){
        PieceColor currentColor = board.getPiece(move.start).pieceColor;
        Position startPosition = null;
        Position endPosition = null;
        if(whoAttack(board,gameNotation).size()>0) {
            startPosition = whoAttack(board, gameNotation).get(0);
        }
        if (kingPosition(board,currentColor) != null) {
            endPosition = kingPosition(board, currentColor);
        }
        return new Move(endPosition,startPosition).getDirection();
    }



    private Position kingPosition(Board board, PieceColor currentColor) {
        Position kingPosition = null;
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if (board.getPiece(new Position(row, col)) != null &&
                        board.getPiece(new Position(row, col)).pieceColor == currentColor &&
                        board.getPiece(new Position(row, col)).pieceType == PieceType.KING) {
                    kingPosition = new Position(row, col);
                    break;
                }
            }
        }
        return kingPosition;
    }

    private List<Position> whoAttack(Board board, GameNotation gameNotation) {
        PieceColor currentColor = board.getPiece(move.start).pieceColor;
        Piece tmpPiece = board.getPiece(move.start);
        board.putPiece(null,move.start);

        List<Position> whoAttack = new ArrayList<>();
        if (kingPosition(board,currentColor) != null) {
            Position endPosition = kingPosition(board,currentColor);
            for (int row = 0; row < 8; row++) {
                for (int col = 0; col < 8; col++) {
                    Position startPosition = new Position(row, col);
                    if (board.getPiece(startPosition) != null &&

                            gameNotation.getNotation().size() != 0 &&
                           // gameNotation.getLastPiece().pieceColor == currentColor && //todo
                            gameNotation.checkLastActionType() != ActionType.CHECK &&
                            board.getPiece(startPosition).pieceType != PieceType.KING &&
                            board.getPiece(startPosition).pieceType != PieceType.KNIGHT &&
                            board.getPiece(startPosition).pieceColor != currentColor) {
                        Move move = new Move(startPosition, endPosition);
                        MoveValidator mv = MoveValidatorFactory.getValidator(move, board, gameNotation);
                        if (mv.isValid()) {
                            whoAttack.add(startPosition);
                        }
                    }
                }
            }
        }
        board.putPiece(tmpPiece,move.start);
        return whoAttack;
    }
}

