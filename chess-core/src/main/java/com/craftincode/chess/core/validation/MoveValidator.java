package com.craftincode.chess.core.validation;

public interface MoveValidator {
    boolean isValid();

    default MoveValidator and(MoveValidator validator){
        MoveValidator me = this;
        return () -> me.isValid() && validator.isValid();
    }
}
