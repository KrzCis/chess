package com.craftincode.chess.core.model.notation;

import com.craftincode.chess.core.model.*;

import java.util.ArrayList;

public class GameNotation {


    private ArrayList<GameActions> notation;

    public GameNotation() {
        this.notation = new ArrayList<>();
    }

    public ArrayList<GameActions> getNotation() {
        return notation;
    }

    public void addNotation(Move move, Board board) {

        if (board.getPiece(move.end) != null) {
            Piece piece = board.getPiece(move.start);
            Piece oponentPiece = board.getPiece(move.end);
            GameActions gameActions = new GameActions(piece, oponentPiece, move, ActionType.CAPTURE);
            this.notation.add(gameActions);
        } else {
            Piece piece = board.getPiece(move.start);
            GameActions gameActions = new GameActions(piece, move, ActionType.MOVE);
            this.notation.add(gameActions);
        }
    }

    public void write() {
       // System.out.println(Arrays.toString(notation.toArray()) + " " + notation.size());
        for (int i = 0; i <notation.size() ; i++) {
            System.out.println(i+" "+notation.get(i));

        }
    }


    public void gameNotationReset(){
        notation.clear();
    }


    public Piece getLastPiece(){
        int index = notation.size();
        GameActions gameActions = notation.get(index-1);
        return gameActions.getPiece();
    }

    public ActionType checkLastActionType() {
        int index = notation.size();
        GameActions gameActions = notation.get(index-1);
        return gameActions.getActionType();
    }

    public Move getLastMove(){
        int index = notation.size();
        GameActions gameActions = notation.get(index-1);
        return gameActions.getMove();
    }

    public void setActionType(ActionType actionType){
        GameActions gameActions = notation.get(notation.size()-1);
        gameActions.setActionType(actionType);
    }


}