package com.craftincode.chess.core.model;

public enum PieceType {
    KING,QUEEN,ROOK,PAWN,BISHOP,KNIGHT
}
