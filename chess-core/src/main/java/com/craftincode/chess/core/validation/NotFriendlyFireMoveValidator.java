package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.Board;
import com.craftincode.chess.core.model.Move;
import com.craftincode.chess.core.model.Piece;

public class NotFriendlyFireMoveValidator implements MoveValidator {

    private Move move;
    private Board board;

    public NotFriendlyFireMoveValidator(Move move, Board board) {
        this.move = move;
        this.board = board;
    }

    @Override
    public boolean isValid() {
        Piece srcPiece = board.getPiece(move.start);
        Piece dstPiece = board.getPiece(move.end);

        if (dstPiece == null) {
            return true;
        }

        return !srcPiece.pieceColor.equals(dstPiece.pieceColor);
    }
}