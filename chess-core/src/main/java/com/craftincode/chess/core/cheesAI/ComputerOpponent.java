package com.craftincode.chess.core.cheesAI;

import com.craftincode.chess.core.model.Board;
import com.craftincode.chess.core.model.Move;
import com.craftincode.chess.core.model.PieceColor;
import com.craftincode.chess.core.model.Position;
import com.craftincode.chess.core.model.notation.GameNotation;
import com.craftincode.chess.core.validation.MoveValidator;
import com.craftincode.chess.core.validation.MoveValidatorFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ComputerOpponent {
    Board board;
    GameNotation gameNotation;

    public ComputerOpponent(Board board, GameNotation gameNotation) {
        this.board = board;
        this.gameNotation = gameNotation;
    }


    private List<Position> getListOfBlackPiecesLeft(Board board) {
        List<Position> position = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Position p = new Position(i, j);
                if (board.getPiece(p) != null && board.getPiece(p).pieceColor == PieceColor.BLACK) {
                    position.add(p);
                }
            }
        }
        return position;
    }

    public Move makeRandomMove(Board board, GameNotation history) {
        List<Position> listOfstartPosition = getListOfBlackPiecesLeft(board); //list of possible black pieces
        List<Position> listOfendPosition = new ArrayList<>();  //new empty list

        Random generator = new Random();

        Position positionOfrandomBlackPiece;
        Position startPosition = null;
        Position endPosition;

        for (int i = 0; i < listOfstartPosition.size(); i++) {
            positionOfrandomBlackPiece = listOfstartPosition.get(generator.nextInt(listOfstartPosition.size())); //get random piece
            for (int row = 0; row < 8; row++) { //iterate whole board and check if move is valid
                for (int col = 0; col < 8; col++) {
                    Position checkPosition = new Position(row, col);
                    Move check = new Move(positionOfrandomBlackPiece, checkPosition);
                    MoveValidator mv = MoveValidatorFactory.getValidator(check, board, history);
                    if (mv.isValid()) {
                        listOfendPosition.add(checkPosition); //possible moves for this piece
                    }
                }
            }
            if (listOfendPosition.size() == 0) { //if piece has no possible moves then remove this piece from list
                listOfstartPosition.remove(positionOfrandomBlackPiece);
                System.out.println("positionOfRandomBlackPiece() :"+ positionOfrandomBlackPiece.toString());
            } else {
                startPosition = positionOfrandomBlackPiece;
                break;
            }}
        System.out.println("listOfendPosition.size() :"+ listOfendPosition.size());


                endPosition = listOfendPosition.get(generator.nextInt(listOfendPosition.size()));


        System.out.println("endPosition :"+ endPosition);


            System.out.println(board.getPiece(startPosition).pieceType.name());

            return new Move(startPosition, endPosition);


        }}



