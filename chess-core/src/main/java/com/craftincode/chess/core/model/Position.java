package com.craftincode.chess.core.model;

import java.util.Objects;

public class Position {
    public final int row, col;

    // "A2"
    public Position(String coords){
        String colStr = coords.substring(0,1);
        String rowStr = coords.substring(1);
        col = colToInt(colStr);
        row = rowToInt(rowStr);
    }

    public Position(int row, int col) {
        this.row = row;
        this.col = col;
    }

    private static int colToInt(String col){
        switch (col){
            case "A" : return 0;
            case "B" : return 1;
            case "C" : return 2;
            case "D" : return 3;
            case "E" : return 4;
            case "F" : return 5;
            case "G" : return 6;
            case "H" : return 7;
            default: throw new RuntimeException("Unknown column "+col);
        }
    }

    private static int rowToInt(String rowStr){
        int row = Integer.parseInt(rowStr);
        if (row > 0 && row <9){
            return 8-row;
        }
        throw new RuntimeException("Unknown row " + rowStr);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Position)) return false;
        Position position = (Position) o;
        return row == position.row &&
                col == position.col;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, col);
    }

    @Override
    public String toString() {
        return "Position{" +
                "row=" + row +
                ", col=" + col +
                '}';
    }
}
