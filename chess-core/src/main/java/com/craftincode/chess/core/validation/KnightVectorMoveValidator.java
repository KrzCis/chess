package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.Move;

public class KnightVectorMoveValidator implements MoveValidator{
    private Move move;

    public KnightVectorMoveValidator(Move move) {
        this.move = move;
    }

    @Override
    public boolean isValid() {
        if(move.verticalShift() == 0 || move.horizontalShift() == 0) return false;
        return (Math.abs(move.verticalShift()) + Math.abs(move.horizontalShift())) == 3;
    }
}
