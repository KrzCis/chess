package com.craftincode.chess.core.model;

public enum GameStatus {
    WON, IN_PROGRESS, DRAW
}
