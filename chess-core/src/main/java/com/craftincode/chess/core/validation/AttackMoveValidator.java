package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.Board;
import com.craftincode.chess.core.model.Move;
import com.craftincode.chess.core.model.Piece;

public class AttackMoveValidator implements MoveValidator {
    private Move move;
    private Board board;

    public AttackMoveValidator(Move move, Board board) {
        this.move = move;
        this.board = board;
    }

    @Override
    public boolean isValid() {
        Piece startPiece = board.getPiece(move.start);
        Piece endPiece = board.getPiece(move.end);
        if (startPiece == null) return false;
        if (endPiece == null) return false;
        return !startPiece.pieceColor.equals(endPiece.pieceColor);
    }
}
