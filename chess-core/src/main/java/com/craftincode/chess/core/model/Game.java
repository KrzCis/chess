package com.craftincode.chess.core.model;

public class Game {
    public final Integer gameId;
    public final Player whitePlayer;
    public final Player blackPlayer;
    public final Player winner;
    public final GameStatus status;
    public final Long gameTime;

    public Game(Integer gameId, Player whitePlayer, Player blackPlayer, Player winner, GameStatus status, Long gameTime) {
        this.gameId = gameId;
        this.whitePlayer = whitePlayer;
        this.blackPlayer = blackPlayer;
        this.winner = winner;
        this.status = status;
        this.gameTime = gameTime;
    }

    public Game(Player whitePlayer, Player blackPlayer) {
        this.gameId = null;
        this.whitePlayer = whitePlayer;
        this.blackPlayer = blackPlayer;
        this.winner = null;
        this.status = GameStatus.IN_PROGRESS;
        this.gameTime = null;
    }

    public PieceColor getColorForPlayer(Player player){
        if(whitePlayer.equals(player)){
            return PieceColor.WHITE;
        }
        return PieceColor.BLACK;
    }

    @Override
    public String toString() {
        return "Game{" +
                "gameId=" + gameId +
                ", whitePlayer=" + whitePlayer +
                ", blackPlayer=" + blackPlayer +
                ", winner=" + winner +
                ", status=" + status +
                ", gameTime=" + gameTime +
                '}';
    }
}