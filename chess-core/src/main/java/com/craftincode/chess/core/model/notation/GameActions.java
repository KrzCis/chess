package com.craftincode.chess.core.model.notation;

import com.craftincode.chess.core.model.Move;
import com.craftincode.chess.core.model.Piece;


public class GameActions {

    private Piece piece;
    private Piece oponentPiece;
    private Move move;
    private ActionType actionType;

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public GameActions(Piece piece, Piece oponentPiece, Move move, ActionType actionType) {
        this.piece = piece;
        this.oponentPiece = oponentPiece;
        this.move = move;
        this.actionType = actionType;
    }

    public GameActions(Piece piece, Move move, ActionType actionType) {
        this.piece = piece;
        this.move = move;
        this.actionType = actionType;
    }

    public Piece getPiece() {
        return piece;
    }

    public Move getMove() {
        return move;
    }

    public ActionType getActionType() {
        return actionType;
    }

    @Override
    public String toString() {
        return "GameActions{" +
                "piece=" + piece +
                ", oponentPiece=" + oponentPiece +
                ", move=" + move +
                ", actionType=" + actionType +
                '}';
    }
}
