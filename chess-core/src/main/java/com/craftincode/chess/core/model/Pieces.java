package com.craftincode.chess.core.model;

import static com.craftincode.chess.core.model.PieceType.*;
import static com.craftincode.chess.core.model.PieceColor.*;

public class Pieces {
    public static final Piece WHITE_KING = new Piece(KING, WHITE);
    public static final Piece WHITE_QUEEN = new Piece(QUEEN, WHITE);
    public static final Piece WHITE_ROOK = new Piece(ROOK, WHITE);
    public static final Piece WHITE_BISHOP = new Piece(BISHOP, WHITE);
    public static final Piece WHITE_KNIGHT = new Piece(KNIGHT, WHITE);
    public static final Piece WHITE_PAWN = new Piece(PAWN, WHITE);
    public static final Piece BLACK_KING = new Piece(KING, BLACK);
    public static final Piece BLACK_QUEEN = new Piece(QUEEN, BLACK);
    public static final Piece BLACK_ROOK = new Piece(ROOK, BLACK);
    public static final Piece BLACK_BISHOP = new Piece(BISHOP, BLACK);
    public static final Piece BLACK_KNIGHT = new Piece(KNIGHT, BLACK);
    public static final Piece BLACK_PAWN = new Piece(PAWN, BLACK);
}
