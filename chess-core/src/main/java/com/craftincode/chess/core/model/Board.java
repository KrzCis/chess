package com.craftincode.chess.core.model;

import static com.craftincode.chess.core.model.Pieces.*;

public class Board {
    private Piece[][] pieces = new Piece[8][8];


    public Piece getPiece(Position position) {
        return pieces[position.row][position.col];
    }

    public void putPiece(Piece piece, Position position) {
        pieces[position.row][position.col] = piece;
    }



    public static Board defaultBoard() {
        Board board = new Board();
        // PUT PAWNS
        for (int i = 0; i < 8; i++) {
            board.pieces[1][i] = BLACK_PAWN;
            board.pieces[6][i] = WHITE_PAWN;
        }
        // put others
        Piece[] firstRow = {BLACK_ROOK, BLACK_KNIGHT, BLACK_BISHOP, BLACK_QUEEN, BLACK_KING, BLACK_BISHOP, BLACK_KNIGHT, BLACK_ROOK};
        Piece[] lastRow = {WHITE_ROOK, WHITE_KNIGHT, WHITE_BISHOP, WHITE_QUEEN, WHITE_KING, WHITE_BISHOP, WHITE_KNIGHT, WHITE_ROOK};

        board.pieces[0] = firstRow;
        board.pieces[7] = lastRow;
        return board;
    }



    public static Board testBoard() {
        Board board = new Board();
        // PUT PAWNS
        for (int i = 0; i < 6; i++) {
            board.pieces[1][i] = BLACK_PAWN;

        }
        // put others
        Piece[] firstRow = {null, BLACK_KNIGHT, null, BLACK_QUEEN, BLACK_KING, null, BLACK_KNIGHT, null};
        Piece[] lastRow = {WHITE_ROOK, WHITE_KNIGHT,null,null, WHITE_KING,null,null,  WHITE_ROOK};

        board.pieces[0] = firstRow;
        board.pieces[7] = lastRow;

        board.pieces[6][6] = BLACK_PAWN;
        board.pieces[2][2] = WHITE_PAWN;
        board.pieces[3][3] = WHITE_PAWN;
        board.pieces[4][4] = WHITE_PAWN;
        return board;
    }


    public void applyMove(Move move) {
        Piece piece = getPiece(move.start);
        putPiece(piece, move.end);
        putPiece(null, move.start);
    }

    public boolean isFieldEmpty(Position position) {
        return pieces[position.row][position.col] == null;
    }


}
