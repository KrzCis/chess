package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.*;
import com.craftincode.chess.core.model.notation.GameNotation;

import java.util.HashSet;
import java.util.Set;


public class FieldNotUnderAttackValidator implements MoveValidator {


    private Board board;
    private Move move;
    private GameNotation gameNotation;

    public FieldNotUnderAttackValidator(Board board, Move move, GameNotation gameNotation) {
        this.board = board;
        this.move = move;
        this.gameNotation = gameNotation;
    }

    @Override
    public boolean isValid() {
        return fieldNotUnderAttack(move, board).contains(move.end);
    }

    private Set<Position> fieldNotUnderAttack(Move move, Board board) {
        Piece kingPiece = board.getPiece(move.start);
        PieceColor currentColor = board.getPiece(move.start).pieceColor;
        PieceColor oppositeColor;
        if (currentColor == PieceColor.BLACK) {
            oppositeColor = PieceColor.BLACK;
        } else {
            oppositeColor = PieceColor.WHITE;
        }
        Piece tmpPiece = new Piece(PieceType.PAWN, oppositeColor);


        Set<Position> fieldToCheck = new HashSet<>();
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                Position p = new Position(row, col);
                fieldToCheck.add(p);
            }
        }

        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                Position startPosition = new Position(row, col);

                if (board.getPiece(startPosition) != null &&
                        board.getPiece(startPosition).pieceColor != currentColor &&
                        board.getPiece(startPosition).pieceType == PieceType.KING &&
                        move.start != startPosition) {

                    int rowR = startPosition.row;
                    int colR = startPosition.col;

                    for (int i = (rowR - 1); i < (rowR + 2); i++) {
                        for (int j = (colR - 1); j < (colR + 2); j++) {
                            Position positionToRemove = new Position(i, j);
                            fieldToCheck.remove(positionToRemove);
                        }
                    }
                }

                if (board.getPiece(startPosition) != null &&
                        board.getPiece(startPosition).pieceType != PieceType.KING &&
                        board.getPiece(startPosition).pieceColor != currentColor) {

                    board.putPiece(null, move.start);

                    for (int row1 = 0; row1 < 8; row1++) {
                        for (int col1 = 0; col1 < 8; col1++) {
                            Position endPosition = new Position(row1, col1);
                            Piece savePiece = board.getPiece(endPosition);
                            board.putPiece(tmpPiece, endPosition);


                            Move checkMove = new Move(startPosition, endPosition);
                            MoveValidator mv = MoveValidatorFactory.getValidator(checkMove, board, gameNotation);
                            if (mv.isValid()) {
                                fieldToCheck.remove(endPosition);
                            }
                            board.putPiece(savePiece, endPosition);
                        }
                    }
                    board.putPiece(kingPiece, move.start);
                }
            }
        }
        return fieldToCheck;
    }
}