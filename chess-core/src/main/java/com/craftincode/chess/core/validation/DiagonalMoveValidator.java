package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.Move;

public class DiagonalMoveValidator implements MoveValidator {
    private Move move;

    public DiagonalMoveValidator(Move move) {
        this.move = move;
    }

    @Override
    public boolean isValid() {
        return Math.abs(move.horizontalShift()) == Math.abs(move.verticalShift());
    }
}
