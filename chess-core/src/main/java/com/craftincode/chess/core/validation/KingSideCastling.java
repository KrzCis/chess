package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.*;
import com.craftincode.chess.core.model.notation.GameActions;
import com.craftincode.chess.core.model.notation.GameNotation;

import java.util.ArrayList;

public class KingSideCastling implements MoveValidator {
    private Move move;
    private GameNotation gameNotation;
    private Board board;
    private PieceColor pieceColor;


    public KingSideCastling(Move move, Board board, GameNotation gameNotation, PieceColor pieceColor) {
        this.move = move;
        this.board = board;
        this.gameNotation = gameNotation;
        this.pieceColor = pieceColor;
    }

    @Override
    public boolean isValid() {
        if (kingWasMoved(gameNotation, pieceColor)) return false;
        if (rookWasMoved(gameNotation, pieceColor)) return false;
        if (pawnOnFieldG(board, move)) return false;
        if (passingFieldUnderAttack(board, move)) return false;
        return move.getDirection() == MoveDirection.E;
    }


    private boolean kingWasMoved(GameNotation notation, PieceColor pieceColor) {
        Position positionWhiteKing = new Position(7, 4);
        Position positionBlackKing = new Position(0, 4);
        Position check = pieceColor.equals(PieceColor.WHITE) ? positionWhiteKing : positionBlackKing;
        ArrayList<GameActions> history = new ArrayList<>(notation.getNotation());
        int count = 0;

        for (int i = 0; i < history.size(); i++) {
            if (history.get(i).getMove().start.equals(check)) {
                count = count + 1;
            }
        }
        if (count > 0) {
            return true;
        } else return false;
    }

    private boolean rookWasMoved(GameNotation notation, PieceColor pieceColor) {
        Position positionWhiteRook = new Position(7, 7);
        Position positionBlackRook = new Position(0, 7);
        Position check = pieceColor.equals(PieceColor.WHITE) ? positionWhiteRook : positionBlackRook;
        ArrayList<GameActions> history = new ArrayList<>(notation.getNotation());
        int count = 0;

        for (int i = 0; i < history.size(); i++) {
            if (history.get(i).getMove().start.equals(check)) {
                count = count + 1;
            }
        }
        if (count > 0) {
            return true;
        } else return false;
    }


    private boolean pawnOnFieldG(Board board, Move move) {
        PieceColor currentColor = board.getPiece(move.start).pieceColor;
        int c = currentColor == PieceColor.WHITE ? 6 : 1;
        Position gField = new Position(c, 6);
        if (board.getPiece(gField) != null &&
                board.getPiece(gField).pieceType == PieceType.PAWN &&
                board.getPiece(gField).pieceColor != currentColor) {
            return true;
        }
        return false;
    }

    private boolean passingFieldUnderAttack(Board board, Move move) {
        boolean underAttack = false;
        PieceColor currentColor = board.getPiece(move.start).pieceColor;
        int c = currentColor == PieceColor.WHITE ? 7 : 0;

        Position positionToCheck = new Position(c, 5);
        Position positionToCheck2 = new Position(c, 4);

        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                Position startPosition = new Position(row, col);
                if (board.getPiece(startPosition) != null &&
                        board.getPiece(startPosition).pieceColor != currentColor &&
                        board.getPiece(startPosition).pieceType != PieceType.KING) {

                    Move checkMove = new Move(startPosition, positionToCheck);
                    MoveValidator mv = MoveValidatorFactory.getValidator(checkMove, board, gameNotation);
                    Move checkMove2 = new Move(startPosition, positionToCheck2);
                    MoveValidator mv2 = MoveValidatorFactory.getValidator(checkMove2, board, gameNotation);


                    if (mv.isValid() || mv2.isValid()) {
                        underAttack = true;
                    }
                }
            }
        }
        if (underAttack) return true;
        else return false;
    }


}