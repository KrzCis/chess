package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.Move;
import com.craftincode.chess.core.model.MoveDirection;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static com.craftincode.chess.core.model.MoveDirection.*;
public class OrthogonalMoveValidator implements MoveValidator {
    private static final Set<MoveDirection> validDirections
            = new HashSet<>(Arrays.asList(N,E,S,W));
    private Move move;

    public OrthogonalMoveValidator(Move move) {
        this.move = move;
    }

    @Override
    public boolean isValid() {
        return validDirections.contains(move.getDirection());
    }

}
