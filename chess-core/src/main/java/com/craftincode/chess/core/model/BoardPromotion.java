package com.craftincode.chess.core.model;

import static com.craftincode.chess.core.model.Pieces.*;

public class BoardPromotion {
    private Piece[] pieces = new Piece[5];


    public Piece getPiece(int position) {
        return pieces[position];
    }

    public void putPiece(Piece piece, int position) {
        pieces[position] = piece;
    }



    public static BoardPromotion setBoardPromotionBlack() {
        BoardPromotion boardPromotion = new BoardPromotion();
        // put elements
        Piece[] blackRow = {BLACK_PAWN, BLACK_ROOK, BLACK_KNIGHT, BLACK_BISHOP, BLACK_QUEEN};
        boardPromotion.pieces = blackRow;
        return boardPromotion;
    }

    public static BoardPromotion setBoardPromotionWhite() {
        BoardPromotion boardPromotion = new BoardPromotion();
        // put elements
        Piece[] whiteRow = {WHITE_PAWN, WHITE_ROOK, WHITE_KNIGHT, WHITE_BISHOP, WHITE_QUEEN};
        boardPromotion.pieces = whiteRow;
        return boardPromotion;
    }
}
