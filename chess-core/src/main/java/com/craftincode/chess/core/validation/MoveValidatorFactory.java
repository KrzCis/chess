package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.*;
import com.craftincode.chess.core.model.notation.GameNotation;

public class MoveValidatorFactory {

    private static final MoveValidator INVALID = () -> false;

    public static MoveValidator getValidator(Move move, Board board, GameNotation gameNotation) {
        Piece piece = board.getPiece(move.start);
        if (piece == null) return INVALID;

        PieceType type = piece.pieceType;
        PieceColor color = piece.pieceColor;

        switch (type) {
            case QUEEN:
                return new MoveValidatorBuilder()
                        .and(new NotFriendlyFireMoveValidator(move, board))
                        .and(new CollisionMoveValidator(move, board))
                        .or(new OrthogonalMoveValidator(move))
                        .or(new DiagonalMoveValidator(move))
                        .and(new KingUnderAttackValidator(board,move,gameNotation))
                        .and(new ExposedKingValidator(board,move,gameNotation))
                        .build();
            case ROOK:
                return new MoveValidatorBuilder()
                        .and(new NotFriendlyFireMoveValidator(move, board))
                        .and(new CollisionMoveValidator(move, board))
                        .and(new OrthogonalMoveValidator(move))
                        .and(new KingUnderAttackValidator(board,move,gameNotation))
                        .and(new ExposedKingValidator(board,move,gameNotation))
                        .build();
            case BISHOP:
                return new MoveValidatorBuilder()
                        .and(new NotFriendlyFireMoveValidator(move, board))
                        .and(new CollisionMoveValidator(move, board))
                        .and(new DiagonalMoveValidator(move))
                        .and(new KingUnderAttackValidator(board,move,gameNotation))
                        .and(new ExposedKingValidator(board,move,gameNotation))
                        .build();
            case PAWN:
                return new MoveValidatorBuilder()
                        .or(
                                new PawnVectorMoveValidator(move, color)
                                        .and(new NotFriendlyFireMoveValidator(move, board))
                                        .and(new CollisionMoveValidator(move, board))
                                        .and(new PawnCollisionMoveValidator(move, board))
                                        .and(new KingUnderAttackValidator(board,move,gameNotation))
                                        .and(new ExposedKingValidator(board,move,gameNotation))
                        )
                        .or(
                                new AttackMoveValidator(move, board)
                                        .and(new PawnAttackVectorMoveValidator(move, board))
                                        .and(new RangeMoveValidator(move, 1))
                                        .and(new KingUnderAttackValidator(board,move,gameNotation))
                                        .and(new ExposedKingValidator(board,move,gameNotation))

                        )
                        .or(
                                new PawnAttackEnPassantValidator(move, board, gameNotation, color)
                                        .and(new RangeMoveValidator(move, 1))
                                        .and(new DiagonalMoveValidator(move))
                                        .and(new KingUnderAttackValidator(board,move,gameNotation))
                                        .and(new ExposedKingValidator(board,move,gameNotation))
                        )

                        .build();


            case KNIGHT:
                return new MoveValidatorBuilder()
                        .and(new KnightVectorMoveValidator(move))
                        .and(new NotFriendlyFireMoveValidator(move, board))
                        .and(new KingUnderAttackValidator(board,move,gameNotation))
                        .and(new ExposedKingValidator(board,move,gameNotation))
                        .build();

            case KING:
                return new MoveValidatorBuilder()
                        .or(
                                (new NotFriendlyFireMoveValidator(move, board))
                                        .and(new CollisionMoveValidator(move, board))
                                        .and(new RangeMoveValidator(move, 1))
                                        .and(new FieldNotUnderAttackValidator(board,move,gameNotation))
                        )
                        .or(
                                (new NotFriendlyFireMoveValidator(move, board))
                                        .and(new CollisionMoveValidator(move, board))
                                        .and(new KingSideCastling(move, board, gameNotation, color))
                                        .and(new FieldNotUnderAttackValidator(board,move,gameNotation))
                                        .and(new RangeMoveValidator(move, 2)))
                        .or(
                                (new NotFriendlyFireMoveValidator(move, board))
                                        .and(new CollisionMoveValidator(move, board))
                                        .and(new QueenSideCastling(move, gameNotation, color, board))
                                        .and(new FieldNotUnderAttackValidator(board,move,gameNotation)))
                                        .and(new RangeMoveValidator(move, 3))
                        .build();

            default:
                return INVALID;
        }
    }

}
