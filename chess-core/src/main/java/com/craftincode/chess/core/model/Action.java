package com.craftincode.chess.core.model;

public class Action {
    public final Integer actionId;
    public final Game game;
    public final Player player;
    public final String name;

    public Action(Integer actionId, Game game, Player player, String name) {
        this.actionId = actionId;
        this.game = game;
        this.player = player;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Action{" +
                "actionId=" + actionId +
                ", game=" + game +
                ", player=" + player +
                ", name='" + name + '\'' +
                '}';
    }
}
