package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.Move;

public class RangeMoveValidator implements MoveValidator {
    private Move move;
    private int range;

    public RangeMoveValidator(Move move, int range) {
        this.move = move;
        this.range = range;
    }

    @Override
    public boolean isValid() {
        return Math.max(
                Math.abs(move.verticalShift()), Math.abs(move.horizontalShift())
        ) <= range;
    }
}
