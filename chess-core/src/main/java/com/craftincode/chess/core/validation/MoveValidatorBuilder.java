package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.Board;
import com.craftincode.chess.core.model.Move;
import com.craftincode.chess.core.model.Position;

import java.util.ArrayList;
import java.util.List;

public class MoveValidatorBuilder {
    private List<MoveValidator> andValidators = new ArrayList<>();
    private List<MoveValidator> orValidators = new ArrayList<>();

    public MoveValidatorBuilder and(MoveValidator validator) {
        andValidators.add(validator);
        return this;
    }

    public MoveValidatorBuilder or(MoveValidator validator) {
        orValidators.add(validator);
        return this;
    }

    public MoveValidator build() {
        MoveValidator orValidator = composeOrValidator();
        MoveValidator andValidator = composeAndValidator();
        return () -> andValidator.isValid() && orValidator.isValid();
    }

    private MoveValidator composeAndValidator() {
        return new MoveValidator() {
            @Override
            public boolean isValid() {
                for (MoveValidator validator : andValidators) {
                    if (!validator.isValid()) return false;
                }
                return true;
            }
        };
    }

    private MoveValidator composeOrValidator() {
        return new MoveValidator() {
            @Override
            public boolean isValid() {
                if(orValidators.size()==0) return true;
                for (MoveValidator validator : orValidators) {
                    if (validator.isValid()) return true;
                }
                return false;
            }
        };
    }

    public static void main(String[] args) {
        Board board = new Board();
        Move move = new Move(new Position("A2"), new Position("A4"));
        MoveValidator rookValidator = new MoveValidatorBuilder()
                .and(new OrthogonalMoveValidator(move))
                .and(new CollisionMoveValidator(move, board))
                .build();

        MoveValidator queenValidator = new MoveValidatorBuilder()
                .or(new DiagonalMoveValidator(move))
                .and(new CollisionMoveValidator(move, board))
                .or(new OrthogonalMoveValidator(move))
                .build();

    }
}
