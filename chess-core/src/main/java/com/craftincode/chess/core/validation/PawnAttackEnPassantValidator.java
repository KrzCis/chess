package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.*;
import com.craftincode.chess.core.model.notation.GameActions;
import com.craftincode.chess.core.model.notation.GameNotation;

import java.util.ArrayList;

public class PawnAttackEnPassantValidator implements MoveValidator {

    private Move move;
    private Board board;
    private GameNotation gameNotation;
    private PieceColor pieceColor;

    public PawnAttackEnPassantValidator(Move move, Board board, GameNotation gameNotation, PieceColor pieceColor) {
        this.move = move;
        this.board = board;
        this.gameNotation = gameNotation;
        this.pieceColor = pieceColor;
    }

    @Override
    public boolean isValid() {
        if (pawnNotOnPosition(pieceColor, move)) return false;
        if (lastMoveNotPawn(gameNotation)) return false;
        if (pawnMovedNotTwo(gameNotation)) return false;
        if(tooBigRange(gameNotation,move)) return false;

        return move.getDirection() == attackDirection(pieceColor, gameNotation, move);
    }

    private boolean pawnNotOnPosition(PieceColor pieceColor, Move move) {
        int row = (pieceColor == PieceColor.WHITE) ? 3 : 4;
        if (move.start.row != row) return true;
        else return false;
    }

    private MoveDirection attackDirection(PieceColor color, GameNotation notation, Move move) {
        if (color == PieceColor.WHITE && attackOnLeft(notation, move)) {
            return MoveDirection.NW;
        } else if (color == PieceColor.WHITE && !attackOnLeft(notation, move)){
            return MoveDirection.NE;
        }
        if((color == PieceColor.BLACK && attackOnLeft(notation, move))) {
            return MoveDirection.SW;
        } else return MoveDirection.SE;
    }

    private boolean lastMoveNotPawn(GameNotation notation) {
        ArrayList<GameActions> history = new ArrayList<>(notation.getNotation());
        if (history.get(history.size()-1).getPiece().pieceType != PieceType.PAWN) return true;
        else return false;
    }

    private boolean pawnMovedNotTwo(GameNotation gameNotation) {
        Move m = gameNotation.getLastMove();
        if (Math.abs(Math.abs(m.start.row) - Math.abs(m.end.row)) == 1) return true;
        else return false;
    }

    private boolean tooBigRange(GameNotation notation, Move move) {
        ArrayList<GameActions> history = new ArrayList<>(notation.getNotation());
        int lastCol = history.get(history.size()-1).getMove().end.col;
        int currentCol = move.start.col;
        if(Math.abs(Math.abs(lastCol)-Math.abs(currentCol))>1) return true;
        else return false;
    }

    private boolean attackOnLeft(GameNotation notation, Move move) {
        ArrayList<GameActions> history = new ArrayList<>(notation.getNotation());
        int lastCol = history.get(history.size()-1).getMove().end.col;
        int currentCol = move.start.col;
        if(Math.abs(lastCol)-Math.abs(currentCol)<0) return true;
        else return false;
    }


}
