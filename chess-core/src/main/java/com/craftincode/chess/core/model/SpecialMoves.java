package com.craftincode.chess.core.model;

import com.craftincode.chess.core.model.notation.GameNotation;
import com.craftincode.chess.core.validation.MoveValidator;
import com.craftincode.chess.core.validation.MoveValidatorFactory;

import java.util.ArrayList;
import java.util.List;

public class SpecialMoves {




    public SpecialMoves() {


    }

    public boolean kingSideCastling(Board board, Move move) {
        int row = board.getPiece(move.start).pieceColor == PieceColor.WHITE ? 7 : 0;
        Position start = new Position(row, 4);
        Position end = new Position(row, 6);
        if (move.start.equals(start) && move.end.equals(end) && board.getPiece(start).pieceType == PieceType.KING) {
            return true;
        } else {
            return false;
        }
    }

    public Position getKingPosition(Board board, PieceColor pieceColor) {
        //give opposite king color than currentColor
        Position kingPosition = null;
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if (board.getPiece(new Position(row, col)) != null &&
                        board.getPiece(new Position(row, col)).pieceType == PieceType.KING &&
                        board.getPiece(new Position(row, col)).pieceColor != pieceColor) {
                    kingPosition = new Position(row, col);
                }
            }
        }
        return kingPosition;
    }

    public List<Position> getAttackingPieces(Board board, Move move, GameNotation gameNotation) {
        List<Position> p = new ArrayList<>();
        PieceColor currentColor = board.getPiece(move.end).pieceColor;
        Position kingPosition = getKingPosition(board, currentColor);

        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if (board.getPiece(new Position(row, col)) != null &&
                        board.getPiece(new Position(row, col)).pieceType != PieceType.KING &&
                        board.getPiece(new Position(row, col)).pieceColor == currentColor) {
                    Position attackingPiece = new Position(row, col);
                    Move checkMove = new Move(attackingPiece, kingPosition);
                    MoveValidator mv = MoveValidatorFactory.getValidator(checkMove, board, gameNotation);
                    if (mv.isValid()) {

                        p.add(attackingPiece);

                    }
                }
            }
        }

        return p;
    }


    public boolean check(Board board, Move move, GameNotation gameNotation) {
        PieceColor currentColor = board.getPiece(move.end).pieceColor;
        Position kingPosition = getKingPosition(board, currentColor);

        boolean check = false;
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if (board.getPiece(new Position(row, col)) != null &&
                        board.getPiece(new Position(row, col)).pieceType != PieceType.KING &&
                        board.getPiece(new Position(row, col)).pieceColor == currentColor) {

                    Move checkMove = new Move(new Position(row, col), kingPosition);
                    MoveValidator mv = MoveValidatorFactory.getValidator(checkMove, board, gameNotation);
                    if (mv.isValid()) {
                        check = true;
                        break;
                    }
                }
            }
        }
        if (check) {
            return true;
        } else return false;
    }


    public boolean checkMate(Board board, Move move, GameNotation gameNotation) {
        PieceColor currentColor = board.getPiece(move.end).pieceColor;
        List<Move> validMoves = new ArrayList<>();
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if (board.getPiece(new Position(row, col)) != null &&
                        board.getPiece(new Position(row, col)).pieceColor != currentColor) {
                    Position startPosition = new Position(row, col);
                    for (int row1 = 0; row1 < 8; row1++) {
                        for (int col1 = 0; col1 < 8; col1++) {
                            Position endPosition = new Position(row1, col1);
                            Move move1 = new Move(startPosition, endPosition);
                            MoveValidator mv = MoveValidatorFactory.getValidator(move1, board, gameNotation);
                            if (mv.isValid()) {
                                validMoves.add(move1);
                                break;
                            }
                        }
                    }
                }
            }
        }

        if (validMoves.size() == 0) {
            return true;
        } else return false;
    }


    public boolean queenSideCastling(Board board, Move move) {
        int row = board.getPiece(move.start).pieceColor == PieceColor.WHITE ? 7 : 0;
        Position start = new Position(row, 4);
        Position end = new Position(row, 1);
        if (move.start.equals(start) && move.end.equals(end) && board.getPiece(start).pieceType == PieceType.KING) {
            return true;
        } else {
            return false;
        }
    }

    public boolean promotion(Board board, Move move, PieceColor pieceColor) {
        int endingRow = pieceColor.equals(PieceColor.WHITE) ? 0 : 7;
        PieceType pawn = PieceType.PAWN;
        if (board.getPiece(move.start).pieceType == pawn && move.end.row == endingRow) return true;
        else return false;
    }

    public boolean enpassant(Board board, Move move) {
        PieceType pawn = PieceType.PAWN;
        if (board.getPiece(move.start).pieceType == pawn && board.getPiece(move.end) == null &&
                (move.getDirection() == MoveDirection.NE || move.getDirection() == MoveDirection.NW ||
                        move.getDirection() == MoveDirection.SE || move.getDirection() == MoveDirection.SW)) {
            return true;
        } else return false;
    }

}
