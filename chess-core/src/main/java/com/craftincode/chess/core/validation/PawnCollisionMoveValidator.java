package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.*;

public class PawnCollisionMoveValidator implements MoveValidator {

    private Move move;
    private Board board;


    public PawnCollisionMoveValidator(Move move, Board board) {
        this.move = move;
        this.board = board;
    }


    @Override
    public boolean isValid() {
        MoveDirection direction = move.getDirection();

        int rowDelta = direction.rowDelta;
        int stepsLimit = Math.abs(move.verticalShift());
        for (int i = 0; i < stepsLimit; i++, rowDelta += direction.rowDelta) {
            if (board.getPiece(new Position(move.start.row + rowDelta, move.start.col)) != null) {
                return false;
            }
        }
        return true;
    }

}

