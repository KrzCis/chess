package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.*;

import java.util.Arrays;
import java.util.List;

public class  PawnAttackVectorMoveValidator implements MoveValidator {
    private Move move;
    private Board board;

    public PawnAttackVectorMoveValidator(Move move, Board board) {
        this.move = move;
        this.board = board;
    }

    @Override
    public boolean isValid() {
        Piece piece = board.getPiece(move.start);
        if (piece == null) return false;

        List<MoveDirection> whiteDirections = Arrays.asList(MoveDirection.NE, MoveDirection.NW);
        List<MoveDirection> blackDirections = Arrays.asList(MoveDirection.SE, MoveDirection.SW);
        if (piece.pieceColor.equals(PieceColor.WHITE)) {
            return whiteDirections.contains(move.getDirection());
        } else {
            return blackDirections.contains(move.getDirection());
        }
    }
}
