package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.*;
import com.craftincode.chess.core.model.notation.GameActions;
import com.craftincode.chess.core.model.notation.GameNotation;

import java.util.ArrayList;

public class QueenSideCastling implements MoveValidator {

    private Move move;
    private GameNotation gameNotation;
    private PieceColor pieceColor;
    private Board board;


    public QueenSideCastling(Move move, GameNotation gameNotation, PieceColor pieceColor, Board board) {
        this.move = move;
        this.gameNotation = gameNotation;
        this.pieceColor = pieceColor;
        this.board=board;
    }

    @Override
    public boolean isValid() {
        if (kingWasMoved(gameNotation, pieceColor)) return false;
        if (rookWasMoved(gameNotation, pieceColor)) return false;
        if (pawnOnFieldB(board,move)) return false;
        if (passingFieldUnderAttack(board,move)) return false;
        return move.end.col==1 && move.end.row ==color(board);

    }

    private int color(Board board){
        if(board.getPiece(move.start).pieceColor==PieceColor.WHITE)
            return 7;
        else return 0;
    }

    private boolean pawnOnFieldB(Board board, Move move) {
        PieceColor currentColor = board.getPiece(move.start).pieceColor;
        int c = currentColor == PieceColor.WHITE ? 6 : 1;
        Position gField = new Position(c, 1);
        if (board.getPiece(gField) != null &&
                board.getPiece(gField).pieceType == PieceType.PAWN &&
                board.getPiece(gField).pieceColor != currentColor) {
            return true;
        }


    return false;
    }

    private boolean kingWasMoved(GameNotation notation, PieceColor pieceColor) {
        Position positionWhiteKing = new Position(7, 4);
        Position positionBlackKing = new Position(0, 4);
        Position check = pieceColor.equals(PieceColor.WHITE) ? positionWhiteKing : positionBlackKing;
        ArrayList<GameActions> history = new ArrayList<>(notation.getNotation());
        int count = 0;

        for (int i = 0; i < history.size(); i++) {
            if (history.get(i).getMove().start.equals(check)) {
                count = count + 1;
            }
        }
        if (count > 0) {
            return true;
        } else return false;
    }

    private boolean rookWasMoved(GameNotation notation, PieceColor pieceColor) {
        Position positionWhiteRook = new Position(7, 0);
        Position positionBlackRook = new Position(0, 0);
        Position check = pieceColor.equals(PieceColor.WHITE) ? positionWhiteRook : positionBlackRook;
        ArrayList<GameActions> history = new ArrayList<>(notation.getNotation());
        int count = 0;

        for (int i = 0; i < history.size(); i++) {
            if (history.get(i).getMove().start.equals(check)) {
                count = count + 1;
            }
        }
        if (count > 0) {
            return true;
        } else return false;
    }


    private boolean passingFieldUnderAttack(Board board, Move move) {
        boolean underAttack = false;
        PieceColor currentColor = board.getPiece(move.start).pieceColor;
        int c = currentColor == PieceColor.WHITE ? 7 : 0;

        Position positionToCheck = new Position(c, 2);
        Position positionToCheck2 = new Position(c, 3);
        Position positionToCheck3 = new Position(c, 4);

        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                Position startPosition = new Position(row, col);
                if (board.getPiece(startPosition) != null &&
                        board.getPiece(startPosition).pieceColor != currentColor &&
                        board.getPiece(startPosition).pieceType != PieceType.KING) {

                    Move checkMove = new Move(startPosition, positionToCheck);
                    MoveValidator mv = MoveValidatorFactory.getValidator(checkMove, board, gameNotation);

                    Move checkMove2 = new Move(startPosition, positionToCheck2);
                    MoveValidator mv2 = MoveValidatorFactory.getValidator(checkMove2, board, gameNotation);

                    Move checkMove3 = new Move(startPosition, positionToCheck3);
                    MoveValidator mv3 = MoveValidatorFactory.getValidator(checkMove3, board, gameNotation);

                    if (mv.isValid() || mv2.isValid() || mv3.isValid()) {
                        underAttack = true;
                    }
                }
            }}
        if(underAttack) return true;
        else return false;
    }

}