package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.Board;
import com.craftincode.chess.core.model.Move;
import com.craftincode.chess.core.model.MoveDirection;
import com.craftincode.chess.core.model.Position;

public class CollisionMoveValidator implements MoveValidator {
    private Move move;
    private Board board;

    public CollisionMoveValidator(Move move, Board board) {
        this.move = move;
        this.board = board;
    }

    @Override
    public boolean isValid() {
        MoveDirection direction = move.getDirection();
        if (direction == MoveDirection.INCORRECT) {
            return false;
        } else {
            int rowDelta = direction.rowDelta;
            int colDelta = direction.colDelta;
            int stepsLimit = Math.max(Math.abs(move.horizontalShift()), Math.abs(move.verticalShift()));
            for (int i = 1; i < stepsLimit; i++, rowDelta += direction.rowDelta, colDelta += direction.colDelta) {
                if (board.getPiece(new Position(move.start.row + rowDelta, move.start.col + colDelta)) != null) {
                    return false;
                }
            }
            return true;
        }

    }}