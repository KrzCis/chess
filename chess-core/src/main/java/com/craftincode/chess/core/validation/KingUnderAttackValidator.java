package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.*;
import com.craftincode.chess.core.model.notation.ActionType;
import com.craftincode.chess.core.model.notation.GameActions;
import com.craftincode.chess.core.model.notation.GameNotation;

import java.util.ArrayList;
import java.util.List;

public class KingUnderAttackValidator implements MoveValidator {

    private Board board;
    private Move move;
    private GameNotation gameNotation;


    public KingUnderAttackValidator(Board board, Move move, GameNotation gameNotation) {
        this.board = board;
        this.move = move;
        this.gameNotation = gameNotation;
    }

    @Override
    public boolean isValid() {
        if (whoAttack(board, gameNotation).size() == 1) {
            return saveTheKingFields(board, gameNotation).contains(move.end);
        }
        if (whoAttack(board, gameNotation).size() == 2) return false;

        return true;
    }


    private Position kingPosition(Board board, GameNotation gameNotation, PieceColor attackerColor) {
        ArrayList<GameActions> historyOfGame = new ArrayList<>(gameNotation.getNotation());
        int index = historyOfGame.size() - 1;
        Position kingPosition = null;

        if (historyOfGame.size() > 0 &&
                historyOfGame.get(index).getActionType() == ActionType.CHECK) {
            attackerColor = historyOfGame.get(index).getPiece().pieceColor;
        }

         for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if (historyOfGame.size() > 0 &&
                        historyOfGame.get(index).getActionType() == ActionType.CHECK &&
                        board.getPiece(new Position(row, col)) != null &&
                        board.getPiece(new Position(row, col)).pieceColor != attackerColor &&
                        board.getPiece(new Position(row, col)).pieceType == PieceType.KING) {
                    kingPosition = new Position(row, col);
                    break;
                }
            }
        }
        return kingPosition;
    }

    private List<Position> whoAttack(Board board, GameNotation gameNotation) {
        PieceColor currentColor = board.getPiece(move.start).pieceColor;
        List<Position> whoAttack = new ArrayList<>();
        if (kingPosition(board, gameNotation,PieceColor.WHITE) != null) {
            Position endPosition = kingPosition(board, gameNotation,PieceColor.WHITE);
            for (int row = 0; row < 8; row++) {
                for (int col = 0; col < 8; col++) {
                    Position startPosition = new Position(row, col);
                    if (board.getPiece(startPosition) != null &&
                            board.getPiece(startPosition).pieceType != PieceType.KING &&
                            board.getPiece(startPosition).pieceColor != currentColor) {
                        Move move = new Move(startPosition, endPosition);
                        MoveValidator mv = MoveValidatorFactory.getValidator(move, board, gameNotation);
                        if (mv.isValid()) {
                            whoAttack.add(startPosition);
                        }
                    }
                }
            }
        }
        return whoAttack;
    }


    private List<Position> saveTheKingFields(Board board, GameNotation gameNotation) {
        List<Position> saveKing = new ArrayList<>();
        if (whoAttack(board, gameNotation).size() == 1 && kingPosition(board, gameNotation,PieceColor.WHITE) != null) {
            if (board.getPiece(whoAttack(board, gameNotation).get(0)).pieceType == PieceType.KNIGHT) {
                return whoAttack(board, gameNotation);
            } else {
                Move move = new Move(whoAttack(board, gameNotation).get(0), kingPosition(board, gameNotation,PieceColor.WHITE));
                saveKing.add(whoAttack(board, gameNotation).get(0));
                int rowDelta = move.getDirection().rowDelta;
                int colDelta = move.getDirection().colDelta;
                int stepsLimit = Math.max(Math.abs(move.horizontalShift()), Math.abs(move.verticalShift()));
                for (int i = 1; i < stepsLimit; i++, rowDelta += move.getDirection().rowDelta, colDelta += move.getDirection().colDelta) {
                    Position p = new Position(move.start.row + rowDelta, move.start.col + colDelta);
                    saveKing.add(p);
                }
            }
        }
        return saveKing;
    }
}

