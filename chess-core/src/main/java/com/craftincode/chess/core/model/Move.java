package com.craftincode.chess.core.model;

public class Move {
    public final Position start, end;

    public Move(Position start, Position end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public String toString() {
        return "Move{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }

    // N,E,S,W,NE,NW,SE,SW
    public MoveDirection getDirection() {

        if (verticalShift() < 0 && horizontalShift() == 0) {
            return MoveDirection.N;
        } else if (verticalShift() == 0 && horizontalShift() > 0) {
            return MoveDirection.E;
        } else if (verticalShift() > 0 && horizontalShift() == 0) {
            return MoveDirection.S;
        } else if (verticalShift() == 0 && horizontalShift() < 0) {
            return MoveDirection.W;
        } else if (verticalShift() < 0 && horizontalShift() > 0 && horizontalShift() + verticalShift()==0){
            return MoveDirection.NE;
        } else if (verticalShift() < 0 && horizontalShift() < 0 && verticalShift()-horizontalShift()==0) {
            return MoveDirection.NW;
        } else if (verticalShift() > 0 && horizontalShift() > 0 && verticalShift()-horizontalShift()==0) {
            return MoveDirection.SE;
        } else if (verticalShift() > 0 && horizontalShift() < 0 && verticalShift()+horizontalShift()==0) {
            return MoveDirection.SW;
        } else if (Math.abs(verticalShift()) - Math.abs(horizontalShift()) != 0 && (verticalShift() != 0 && horizontalShift() != 0)) {
            return MoveDirection.INCORRECT;
        }

        return null;
    }

    public int verticalShift() {
        return end.row - start.row;
    }

    public int horizontalShift() {
        return end.col - start.col;
    }
}
