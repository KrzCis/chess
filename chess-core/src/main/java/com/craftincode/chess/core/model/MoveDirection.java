package com.craftincode.chess.core.model;

public enum MoveDirection {
    N(-1,0), E(0,1), S(1,0), W(0,-1), NE(-1,1), NW(-1,-1), SE(1,1), SW(1,-1), INCORRECT(0,0);

    public int rowDelta,colDelta;

    MoveDirection(int rowDelta, int colDelta) {
        this.rowDelta = rowDelta;
        this.colDelta = colDelta;
    }
}
