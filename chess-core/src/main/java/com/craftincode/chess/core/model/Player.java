package com.craftincode.chess.core.model;

public class Player { //POJO
    public final Integer playerId;
    public final String username;
    public final String password;

    public Player(Integer playerId, String username, String password) {
        this.playerId = playerId;
        this.username = username;
        this.password = password;
    }

    public Player(String username, String password) {
        this(null,username,password);
    }

    @Override
    public String toString() {
        return "Player{" +
                "playerId=" + playerId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
