package com.craftincode.chess.core.model;

import com.craftincode.chess.core.model.Move;
import com.craftincode.chess.core.model.Position;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

@RunWith(Parameterized.class)
public class MoveTest {
    @Parameterized.Parameter(0)
    public String startPosition;
    @Parameterized.Parameter(1)
    public String endPosition;
    @Parameterized.Parameter(2)
    public int expectedVerticalShift;
    @Parameterized.Parameter(3)
    public int expectedHorizontalShift;

    @Parameterized.Parameters(name = "{index}: MOVE({0},{1})")
    public static List data() {
        return Arrays.asList(new Object[][]{
                {"A2", "A4", -2,0},
                {"A4", "A2", 2,0},
                {"A8", "B7", 1,1},
                {"B8", "A7", 1,-1}
        });
    }

    @Test
    public void shift_parameterized(){
        Move move = new Move(new Position(startPosition), new Position(endPosition));
        Assert.assertEquals("Vertical shift incorrect", expectedVerticalShift,move.verticalShift());
        Assert.assertEquals("Horizontal shift incorrect", expectedHorizontalShift,move.horizontalShift());
    }
}
