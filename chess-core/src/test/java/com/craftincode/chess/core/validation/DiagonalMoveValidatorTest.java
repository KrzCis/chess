package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.Move;
import com.craftincode.chess.core.model.Position;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

@RunWith(Parameterized.class)
public class DiagonalMoveValidatorTest {

    @Parameterized.Parameter
    public String startPosition;
    @Parameterized.Parameter(1)
    public String endPosition;
    @Parameterized.Parameter(2)
    public boolean validMove;


    @Parameterized.Parameters(name = "{index}: MOVE({0},{1})")
    public static List data() {
        return Arrays.asList(new Object[][]{
                {"A2", "D5", true},
                {"C4", "E2", true},
                {"B4", "D5", false},
                {"G2", "E4", true}
        });
    }

    @Test
    public void diagonal_validator(){
        Move move = new Move(new Position(startPosition), new Position(endPosition));
        DiagonalMoveValidator diagonalMoveValidator = new DiagonalMoveValidator(move);
        Assert.assertEquals(validMove, diagonalMoveValidator.isValid());
    }
}
