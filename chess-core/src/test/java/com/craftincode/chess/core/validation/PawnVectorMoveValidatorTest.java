package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.Board;
import com.craftincode.chess.core.model.Move;
import com.craftincode.chess.core.model.Position;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class PawnVectorMoveValidatorTest {
    private Move move;
    private Board board;
    private Position position;
    private boolean expectedResult;


    public PawnVectorMoveValidatorTest(Move move, Board board, Position position, boolean expectedResult) {
        this.move = move;
        this.board = board;
        this.position = position;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters//todo źle...
    public static Collection data() {
        return Arrays.asList(new Object[][]{
                {new Move(new Position("A2"), new Position("A3")), true},
                {new Move(new Position("A2"), new Position("A7")), false}

        });
    }

    @Test
    public void isValid_parametrized() {
        PawnVectorMoveValidator p = new PawnVectorMoveValidator(move, board.getPiece(move.start).pieceColor);
        Assert.assertEquals(expectedResult, p.isValid());
    }
}
