package com.craftincode.chess.core.model;

import com.craftincode.chess.core.validation.MoveValidator;
import com.craftincode.chess.core.validation.OrthogonalMoveValidator;
import org.junit.Assert;
import org.junit.Test;

public class OrthogonalMoveValidatorTest {


    @Test
    public void isOrthogonalMoveValidatorTest_then_not() {
        Move move = new Move(new Position("A1"), new Position("B2"));
        MoveValidator mv = new OrthogonalMoveValidator(move);
        Assert.assertFalse(mv.isValid());

    }
    @Test
    public void isOrthogonalMoveValidatorTest_then_vertical_right() {
        Move move = new Move(new Position("A1"), new Position("A2"));
        MoveValidator mv = new OrthogonalMoveValidator(move);
        Assert.assertTrue(mv.isValid());
    }

    @Test
    public void isOrthogonalMoveValidatorTest_then_vertical_left() {
        Move move = new Move(new Position("A2"), new Position("A1"));
        MoveValidator mv = new OrthogonalMoveValidator(move);
        Assert.assertTrue(mv.isValid());


    }
    @Test
    public void isOrthogonalMoveValidatorTest_then_horizontally_up() {
        Move move = new Move(new Position("A2"), new Position("B2"));
        MoveValidator mv = new OrthogonalMoveValidator(move);
        Assert.assertTrue(mv.isValid());


    }
    @Test
    public void isOrthogonalMoveValidatorTest_then_horizontally_down() {
        Move move = new Move(new Position("B2"), new Position("A2"));
        MoveValidator mv = new OrthogonalMoveValidator(move);
        Assert.assertTrue(mv.isValid());


    }
}
