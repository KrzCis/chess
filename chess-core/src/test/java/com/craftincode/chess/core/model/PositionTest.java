package com.craftincode.chess.core.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

@RunWith(Parameterized.class)
public class PositionTest {

    @Parameterized.Parameter(0)
    public String positionString;
    @Parameterized.Parameter(1)
    public int row;
    @Parameterized.Parameter(2)
    public int col;

    @Parameterized.Parameters(name = "{index}: Position:{0}->r{1},c{2})")
    public static List data() {
        return Arrays.asList(new Object[][]{
                {"A2", 6, 0},
                {"A4", 4, 0},
                {"A8", 0, 0},
                {"B8", 0, 1}
        });
    }

    @Test
    public void test_parameterized() {
        Position position = new Position(positionString);
        Assert.assertEquals("Row incorrect", row, position.row);
        Assert.assertEquals("Col incorrect", col, position.col);
    }

}
