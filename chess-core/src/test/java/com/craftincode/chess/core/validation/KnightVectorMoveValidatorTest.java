package com.craftincode.chess.core.validation;

import com.craftincode.chess.core.model.Move;
import com.craftincode.chess.core.model.Position;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class KnightVectorMoveValidatorTest {
    private Move move;
    private boolean expectedResult;

    public KnightVectorMoveValidatorTest(Move move, boolean expectedResult) {
        this.move = move;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Collection data() {

        return Arrays.asList(new Object[][]{
                {new Move(new Position("D4"), new Position("B3")), true},
                {new Move(new Position("D4"), new Position("F5")), true},
                {new Move(new Position("D4"), new Position("F3")), true},
                {new Move(new Position("D4"), new Position("C6")), true},
                {new Move(new Position("D4"), new Position("B5")), true},
                {new Move(new Position("D4"), new Position("E6")), true},
                {new Move(new Position("D4"), new Position("C2")), true},
                {new Move(new Position("D4"), new Position("E2")), true},

                {new Move(new Position("H1"), new Position("A1")), false},
                {new Move(new Position("H1"), new Position("H8")), false},
                {new Move(new Position("A8"), new Position("A1")), false},
                {new Move(new Position("A8"), new Position("H8")), false},
                {new Move(new Position("H1"), new Position("A8")), false},
                {new Move(new Position("A8"), new Position("H1")), false},
                {new Move(new Position("E5"), new Position("H3")), false}
        });
    }

    @Test
    public void isValid_parametrized() {
        KnightVectorMoveValidator k = new KnightVectorMoveValidator(move);
        Assert.assertEquals(expectedResult, k.isValid());
    }
}
