package com.craftincode.chess.core.model;

import com.craftincode.chess.core.validation.CollisionMoveValidator;
import com.craftincode.chess.core.validation.MoveValidator;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

@RunWith(Parameterized.class)
public class CollisionMoveValidatorTest {

    private static Board sampleBoard;
    @Parameterized.Parameter(0)
    public String posStartString;
    @Parameterized.Parameter(1)
    public String posEndString;
    @Parameterized.Parameter(2)
    public boolean expectedResult;

    @Parameterized.Parameters(name = "{index}: Collision:{0}->{1} = {2})")
    public static List data() {
        return Arrays.asList(new Object[][]{
                {"D4", "D6", true},
                {"D4", "D7", false},
                {"D4", "F6", true},
                {"D4", "G7", false},
                {"D4", "F4", true},
                {"D4", "G4", false},
                {"D4", "F2", true},
                {"D4", "G1", false},
                {"D4", "D2", true},
                {"D4", "D1", false},
                {"D4", "B2", true},
                {"D4", "A1", false},
                {"D4", "B4", true},
                {"D4", "A4", false},
                {"D4", "B6", true},
                {"D4", "A7", false}
        });
    }

    @BeforeClass
    public static void prepareBoard(){
        sampleBoard = new Board();
        Piece somePiece = new Piece(PieceType.PAWN,PieceColor.WHITE);
        // main actor
        sampleBoard.putPiece(somePiece,  new Position("D4"));

        // collision actors
        sampleBoard.putPiece(somePiece,  new Position("B6"));
        sampleBoard.putPiece(somePiece,  new Position("D6"));
        sampleBoard.putPiece(somePiece,  new Position("F6"));
        sampleBoard.putPiece(somePiece,  new Position("F4"));
        sampleBoard.putPiece(somePiece,  new Position("F2"));
        sampleBoard.putPiece(somePiece,  new Position("D2"));
        sampleBoard.putPiece(somePiece,  new Position("B2"));
        sampleBoard.putPiece(somePiece,  new Position("B4"));

    }
    @Test
    public void test_parameterized() {
        Move move = new Move(new Position(posStartString),new Position(posEndString));
        MoveValidator mv = new CollisionMoveValidator(move,sampleBoard);
        Assert.assertEquals(expectedResult,mv.isValid());
    }
}
