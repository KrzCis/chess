package com.craftincode.chess.desktop.gui.panels;

import com.craftincode.chess.core.model.PieceColor;
import com.craftincode.chess.core.model.PieceType;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;

import static com.craftincode.chess.core.model.PieceType.*;


public class PromotionPanel extends JPanel {

    private JButton[] fieldButtons;
    private ActionListener controller;
    private Font buttonsFont = new Font("Serif", Font.BOLD, 31);
    private Image img;

    public PromotionPanel(ActionListener controller) {
        this.controller = controller;
        initialize();
    }


    private void initialize() {
        this.setLayout(new GridLayout(1, 5, 0, 0));
        fieldButtons = fillWithButtons();
        setActionCommandsForButtons(fieldButtons);
        addActionListenerForButtons(fieldButtons, controller);
        setFontForButtons(fieldButtons, buttonsFont);
    }


    private void setFontForButtons(JButton[] buttons, Font font) {
        for (int row = 0; row < buttons.length; row++) {
            buttons[row].setFont(font);
        }
    }


    private void addActionListenerForButtons(JButton[] buttons, ActionListener actionListener) {
        for (int row = 0; row < buttons.length; row++) {
            buttons[row].addActionListener(actionListener);
        }
    }

    private void setActionCommandsForButtons(JButton[] buttons) {
        PieceType[] element = {PAWN, ROOK, KNIGHT, BISHOP, QUEEN};
        for (int row = 0; row < buttons.length; row++) {
            buttons[row].setActionCommand("PROMOTION_" + element[row]);
        }
    }

    private JButton[] fillWithButtons() {
        JButton[] buttons = new JButton[5];
        JButton button;
        for (int row = 0; row < 5; row++) {
            button = new JButton();
            buttons[row] = button;
            this.add(button);

        }
        return buttons;
    }

    public void changeButtonText(int position, String text) {
        fieldButtons[position].setText(text);
    }


    public void changeButtonIcon(int position, PieceColor color, PieceType type) {
        String resource = color.toString().toUpperCase() + "_" + type.toString().toUpperCase() + ".png";
        try {
            InputStream path = this.getClass().getClassLoader().getResourceAsStream(resource);
            img = ImageIO.read(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        fieldButtons[position].setIcon(new ImageIcon(img));
    }

}