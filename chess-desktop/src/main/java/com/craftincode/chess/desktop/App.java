package com.craftincode.chess.desktop;

import com.craftincode.chess.desktop.controler.Controller;
import com.craftincode.chess.desktop.gui.GUI;

public class App {
    public static void main(String[] args) {
        Controller c = new Controller();
        GUI gui = new GUI(c);
        c.setGui(gui);
        c.start();




    }
}
