package com.craftincode.chess.desktop.controler;

import com.craftincode.chess.core.model.Piece;

import java.io.IOException;
import java.util.Properties;

public class UnicodeChess {
    private static Properties pieceUnicodes;

    static {
        pieceUnicodes = new Properties();
        try {
            pieceUnicodes.load(
                    ClassLoader
                            .getSystemClassLoader()
                            .getResourceAsStream("unicode_catalog.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String pieceToUnicode(Piece piece) {
        if(piece==null) return "";
        String key =
                String.format("%s_%s",
                        piece.pieceColor.name().toLowerCase(),
                        piece.pieceType.name().toLowerCase());

        return pieceUnicodes.getProperty(key);

    }
}
