package com.craftincode.chess.desktop.gui.sounds;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class MoveSound {


   public MoveSound() {
        Play();
    }



    public void Play(){
        try{
            Clip clip = AudioSystem.getClip();
            AudioInputStream inputStream = AudioSystem.getAudioInputStream(this.getClass().getResourceAsStream("/move.wav"));
            clip.open(inputStream);
            clip.start();
           // Thread.sleep(clip.getMicrosecondLength()/7000);
        }catch(Exception e){
            System.out.println("Sound Error");
            e.printStackTrace();
        }
    }


}
