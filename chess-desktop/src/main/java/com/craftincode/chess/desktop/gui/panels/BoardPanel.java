package com.craftincode.chess.desktop.gui.panels;

import com.craftincode.chess.core.model.Board;
import com.craftincode.chess.core.model.PieceColor;
import com.craftincode.chess.core.model.PieceType;
import com.craftincode.chess.core.model.Position;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;

public class BoardPanel extends JPanel {

    private static final Color ACTIVE_COLOR = new Color(216, 177, 174);
    private JButton[][] fieldButtons;
    private ActionListener controller;
    private Font fieldButtonsFont = new Font("Serif", Font.BOLD, 31);
    private Image img;
    private Image image;


    public BoardPanel(ActionListener controller) {
        try {
            InputStream path = this.getClass().getClassLoader().getResourceAsStream("board.png");
            image = ImageIO.read(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.controller = controller;
        initialize();
    }

  /*
  private BoardPanel(Image image) {
        this.image = image;
    }
    */

    public void paintComponent(Graphics g) {
        g.drawImage(image, 50, 45, 600, 600, null);
    }

    public void changeButtonText(Position position, String text) {
        fieldButtons[position.row][position.col].setText(text);
    }


    public void click(int row, int col){
        fieldButtons[row][col].doClick(100);
    }

    public void changeButtonIcon(Position position, Board board) {
        if (board.getPiece(position) != null) {
            PieceColor color = board.getPiece(position).pieceColor;
            PieceType type = board.getPiece(position).pieceType;
            String resource = color.toString().toUpperCase()+"_"+type.toString().toUpperCase()+".png";

            try {
                InputStream path = this.getClass().getClassLoader().getResourceAsStream(resource);
                img = ImageIO.read(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
            fieldButtons[position.row][position.col].setIcon(new ImageIcon(img));
        }

        else {
            fieldButtons[position.row][position.col].setIcon(null);
        }
    }

    public void changeButtonBackground(Position position) {
        fieldButtons[position.row][position.col].setContentAreaFilled(true);
    }

    public void changeButtonBackgroundForCheck(Position position) {
        fieldButtons[position.row][position.col].setContentAreaFilled(true);
        fieldButtons[position.row][position.col].setBackground(Color.red);

    }


    private void initialize() {
        this.setLayout(new GridLayout(10, 10, 0, 0));
        JButton[][] buttons = fillWithButtons();
        fieldButtons = extractFieldButtons(buttons);
        setActionCommandsForButtons(fieldButtons);
        addActionListenerForButtons(fieldButtons, controller);
        setFontForButtons(fieldButtons, fieldButtonsFont);
        setBlackWhiteButtons();
    }

    private JButton[][] extractFieldButtons(JButton[][] buttons) {
        JButton[][] fieldButtons = new JButton[8][8];
        for (int row = 0; row < fieldButtons.length; row++) {
            System.arraycopy(buttons[row + 1], 1, fieldButtons[row], 0, fieldButtons[row].length);
        }
        return fieldButtons;
    }


    private void addActionListenerForButtons(JButton[][] buttons, ActionListener actionListener) {
        for (int row = 0; row < buttons.length; row++) {
            for (int col = 0; col < buttons.length; col++) {
                buttons[row][col].addActionListener(actionListener);
            }
        }
    }

    private void setFontForButtons(JButton[][] buttons, Font font) {
        for (int row = 0; row < buttons.length; row++) {
            for (int col = 0; col < buttons.length; col++) {
                buttons[row][col].setFont(font);
            }
        }
    }

    public void setBlackWhiteButtons() {
        for (int row = 0; row < fieldButtons.length; row++) {
            for (int col = 0; col < fieldButtons.length; col++) {
                fieldButtons[row][col].setContentAreaFilled(false);
                fieldButtons[row][col].setBackground(null);
            }
        }
    }


    private void setActionCommandsForButtons(JButton[][] buttons) {
        String actionComand;
        for (int row = 0; row < buttons.length; row++) {
            for (int col = 0; col < buttons.length; col++) {
                actionComand = colToSymbol(col + 1) + rowToSymbol(row + 1);
                buttons[row][col].setActionCommand("FIELD_" + actionComand);
            }
        }
    }

//    private void disableCoordinatesFields(JButton[][] buttons) {
//        for (int i = 0; i < 10; i++) {
//            buttons[0][i].setEnabled(false);
//            buttons[9][i].setEnabled(false);
//            buttons[i][0].setEnabled(false);
//            buttons[i][9].setEnabled(false);
//
//
//            buttons[0][i].setContentAreaFilled(false);
//            buttons[9][i].setContentAreaFilled(false);
//            buttons[i][0].setContentAreaFilled(false);
//            buttons[i][9].setContentAreaFilled(false);
//
//
//        }
//    }

    private void fillCoordinates(JButton[][] buttons) {
        for (int i = 1; i < 9; i++) {
            buttons[0][i].setText(colToSymbol(i));
            buttons[9][i].setText(colToSymbol(i));
            buttons[i][0].setText(rowToSymbol(i));
            buttons[i][9].setText(rowToSymbol(i));
        }
    }

    private JButton[][] fillWithButtons() {
        JButton[][] buttons = new JButton[10][10];
        JButton button;
        for (int row = 0; row < 10; row++) {
            for (int col = 0; col < 10; col++) {
                if (row == 0 || row == 9 || col == 0 || col == 9) {
                    JLabel lb = new JLabel("");
                    this.add(lb);
                } else {
                    button = new JButton();
                    buttons[row][col] = button;
                    this.add(button);
                }
            }
        }
        return buttons;
    }

    private String colToSymbol(int col) {
        if (col < 1 || col > 8) return "";
        return String.valueOf((char) ('A' + col - 1));
    }

    private String rowToSymbol(int row) {
        if (row < 1 || row > 8) return "";
        return String.valueOf((char) ('8' - row + 1));
    }
}
