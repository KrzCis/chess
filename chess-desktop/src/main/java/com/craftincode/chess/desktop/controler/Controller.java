package com.craftincode.chess.desktop.controler;

import com.craftincode.chess.core.model.*;
import com.craftincode.chess.core.model.notation.ActionType;
import com.craftincode.chess.core.model.notation.GameNotation;
import com.craftincode.chess.core.validation.MoveValidator;
import com.craftincode.chess.core.validation.MoveValidatorFactory;
import com.craftincode.chess.desktop.gui.GUI;
import com.craftincode.chess.desktop.gui.sounds.MoveSound;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller implements ActionListener {

    private GUI gui;
    private Board board;
    private boolean firstClick = true;
    private MoveSound moveSound;
    private Position startPosition;
    private Position endPosition;
    private Game game;
    private Player currentPlayer;
    private BoardPromotion boardPromotionBlack;
    private BoardPromotion boardPromotionWhite;


    private GameNotation gameNotation = new GameNotation();
    private SpecialMoves specialMoves = new SpecialMoves();


    public Controller() {
        board = Board.defaultBoard();
        boardPromotionBlack = BoardPromotion.setBoardPromotionBlack();
        boardPromotionWhite = BoardPromotion.setBoardPromotionWhite();

        game = new Game(new Player("player1", "pass1"), new Player("player2", "pass2"));
        currentPlayer = game.whitePlayer;
    }

    public void start() {
        gui.displayBoard(board);
        gui.fillPromotionBoard(boardPromotionBlack);
        gui.display();
    }

    public void setGui(GUI gui) {
        this.gui = gui;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand().substring(0, 5)) {
            case "FIELD":
                actionFromBoard(e);
                break;
            case "PROMO":
                actionFromPromotion(e);
                gui.closePromotion();
                break;
            case "EXIT_":
                gui.exit();
                break;
            case "START":
                board = Board.defaultBoard();
                currentPlayer = game.whitePlayer;
                gameNotation.gameNotationReset();
                gui.displayBoard(board);
                gui.closeEndGame();
                break;
            default:
                System.out.println("error");
        }
    }

    private boolean isRightColorMoved() {
        return game.getColorForPlayer(currentPlayer).equals(board.getPiece(startPosition).pieceColor);
    }

    private void applyMove(Move move) {
        board.applyMove(move);
        gui.displayBoard(board);

    }

    private void actionFromPromotion(ActionEvent e) {
        PieceType pieceType = PieceType.valueOf(e.getActionCommand().substring(10));
        Piece piece = new Piece(pieceType, gameNotation.getLastPiece().pieceColor);
        Position position = gameNotation.getLastMove().end;
        board.putPiece(piece, position);
        gui.displayBoard(board);
    }

    private void actionFromBoard(ActionEvent e) {

        String positionString = e.getActionCommand().substring(6); // "FIELD_A3" -> "A3"
        Position position = new Position(positionString);
        if (firstClick) {
            startPosition = position;
            if (!board.isFieldEmpty(position) && isRightColorMoved()) {
                gui.toggleField(startPosition);
                gui.showAllPossibleMoves(startPosition, board, gameNotation);
                firstClick = !firstClick;
            }
        } else {
            endPosition = position;
            gui.toggleField(startPosition);
            gui.clear();

            Move move = new Move(startPosition, endPosition);



            MoveValidator mv = MoveValidatorFactory.getValidator(move, board, gameNotation);
            if (mv.isValid()) {
                gameNotation.addNotation(move, board);
                if (specialMoves.kingSideCastling(board, move)) {
                    int row = board.getPiece(move.start).pieceColor == PieceColor.WHITE ? 7 : 0;
                    Piece p1 = board.getPiece(new Position(row, 7));
                    board.putPiece(p1, new Position(row, 5));
                    board.putPiece(null, new Position(row, 7));
                    gameNotation.setActionType(ActionType.KINGSIDECASTLING);
                }


                if (specialMoves.queenSideCastling(board, move)) {
                    int row = board.getPiece(move.start).pieceColor == PieceColor.WHITE ? 7 : 0;
                    Piece p1 = board.getPiece(new Position(row, 0));
                    board.putPiece(p1, new Position(row, 2));
                    board.putPiece(null, new Position(row, 0));
                    gameNotation.setActionType(ActionType.QUEENSIDECASTLING);
                }

                if (specialMoves.promotion(board, move, board.getPiece(move.start).pieceColor)) {
                    if (gameNotation.getLastPiece().pieceColor == PieceColor.WHITE) {
                        gui.fillPromotionBoard(boardPromotionWhite);

                    }
                    if (gameNotation.getLastPiece().pieceColor == PieceColor.BLACK) {
                        gui.fillPromotionBoard(boardPromotionBlack);
                    }
                    gameNotation.setActionType(ActionType.PROMOTED);
                    gui.displayPromotion();
                }

                if (specialMoves.enpassant(board, move)) {
                    int row = board.getPiece(move.start).pieceColor == PieceColor.WHITE ? 1 : -1;
                    Position p = new Position(move.end.row + row, move.end.col);
                    gameNotation.setActionType(ActionType.ENPASSANT);
                    board.putPiece(null, p);
                }

                applyMove(move);

               if (specialMoves.check(board, move, gameNotation)) {
                    PieceColor color;
                    if (board.getPiece(move.end).pieceColor == PieceColor.WHITE) {
                        color = PieceColor.WHITE;
                    } else {
                        color = PieceColor.BLACK;
                    }

                    if (gameNotation.getLastMove() != null) {
                        gameNotation.setActionType(ActionType.CHECK);
                        Position p = specialMoves.getKingPosition(board, color);
                        gui.toggleFieldforCheck(p);
                        System.out.println("SZACH!");
                    }
                }

                moveSound = new MoveSound();
                currentPlayer = currentPlayer.equals(game.whitePlayer) ? game.blackPlayer : game.whitePlayer;

                if (specialMoves.checkMate(board, move, gameNotation)) {
                    System.out.println("SZACH MAT!");
                    if(currentPlayer==game.whitePlayer){
                        System.out.println("BLACK IS WINNER");
                        gui.winnerText("BLACK IS WINNER!");
                    }else{
                        System.out.println("WHITE IS WINNER");
                        gui.winnerText("WHITE IS WINNER!");
                    }
                    gui.displayEndGame();

                }

            }
            firstClick = !firstClick;

        }
    }






}