package com.craftincode.chess.desktop.gui.panels;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;


public class EndGamePanel extends JPanel {


    private JButton[][] fieldButtons;
    private ActionListener controller;
    private Font buttonsFont = new Font("Serif", Font.BOLD, 15);

    public EndGamePanel(ActionListener controller) {
        this.controller = controller;
        initialize();
    }


    private void initialize() {
        this.setLayout(new GridLayout(2, 2, 10, 10));
        fieldButtons = fillWithButtons();
        setActionCommandsForButtons(fieldButtons);
        addActionListenerForButtons(fieldButtons, controller);
        setFontForButtons(fieldButtons, buttonsFont);
    }


    private void addActionListenerForButtons(JButton[][] buttons, ActionListener actionListener) {
                buttons[1][0].addActionListener(actionListener);
                buttons[1][1].addActionListener(actionListener);

    }

    private void setFontForButtons(JButton[][] buttons, Font font) {
                buttons[1][0].setFont(font);
                buttons[1][1].setFont(font);
        buttons[1][0].setText("Start new game");
        buttons[1][1].setText("End game");

    }

    public void setWinnerText(String text) {
       fieldButtons[0][0].setText(text);
       fieldButtons[0][0].setEnabled(false);
       fieldButtons[0][1].setEnabled(false);
       // buttons[0][1].setText("WHITE PLAYER");
    }


    private void setActionCommandsForButtons(JButton[][] buttons) {
               buttons[1][0].setActionCommand("START_GAME");
               buttons[1][1].setActionCommand("EXIT_GAME");
    }

    private JButton[][] fillWithButtons() {
        JButton[][] buttons = new JButton[2][2];
        JButton button;
        for (int row = 0; row < 2; row++) {
            for (int col = 0; col < 2; col++) {
              {
                    button = new JButton();
                    buttons[row][col] = button;
                    this.add(button);
                }
            }
        }
        return buttons;
    }


}
