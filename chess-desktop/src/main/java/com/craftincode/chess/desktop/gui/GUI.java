package com.craftincode.chess.desktop.gui;

//import com.craftincode.chess.gui.dto.FieldDTO;

import com.craftincode.chess.core.model.Board;
import com.craftincode.chess.core.model.BoardPromotion;
import com.craftincode.chess.core.model.Move;
import com.craftincode.chess.core.model.Position;
import com.craftincode.chess.core.model.notation.GameNotation;
import com.craftincode.chess.core.validation.MoveValidator;
import com.craftincode.chess.core.validation.MoveValidatorFactory;
import com.craftincode.chess.desktop.gui.panels.BoardPanel;
import com.craftincode.chess.desktop.gui.panels.EndGamePanel;
import com.craftincode.chess.desktop.gui.panels.PromotionPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI {

    private ActionListener controller;
    private JFrame window;
    private JFrame promotionWindow;
    private JFrame endGame;
    private BoardPanel boardPanel;
    private PromotionPanel promotionPanel;
    private EndGamePanel endGamePanel;

    public GUI(ActionListener controller) {
        this.controller = controller;

        window = new JFrame();
        window.setResizable(false);
        window.setMinimumSize(new Dimension(715, 730));
        boardPanel = new BoardPanel(controller);
        window.getContentPane().add(boardPanel);
        window.validate();


        promotionWindow = new JFrame("CHOOSE");
        promotionWindow.setMinimumSize(new Dimension(350, 100));
        promotionPanel = new PromotionPanel(controller);
        promotionWindow.getContentPane().add(promotionPanel);
        promotionWindow.setLocation(200, 200);

        endGame= new JFrame();
        endGame.setMinimumSize(new Dimension(350,100));
        endGamePanel = new EndGamePanel(controller);
        endGame.getContentPane().add(endGamePanel);
        endGame.setLocation(200, 200);


    }

    public void exit(){
        window.setVisible(false);
        window.dispose();
        endGame.setVisible(false);
        endGame.dispose();
        promotionWindow.setVisible(false);
        promotionWindow.dispose();
    }

    public void display() {
        window.setVisible(true);
    }


    public void closeEndGame(){
        endGame.setVisible(false);
    }

    public void displayEndGame(){
        endGame.setVisible(true);
    }

    public void closePromotion() {
        promotionWindow.setVisible(false);
    }


    public void displayPromotion() {
        promotionWindow.setVisible(true);
    }

    public void clear() {
        boardPanel.setBlackWhiteButtons();
    }

    public void toggleField(Position position) {
        boardPanel.changeButtonBackground(position);
    }

    public void toggleFieldforCheck(Position position) {
        boardPanel.changeButtonBackgroundForCheck(position);
    }

    public void showAllPossibleMoves(Position position, Board board, GameNotation gameNotation) {
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                Position checkPosition = new Position(row, col);
                Move check = new Move(position, checkPosition);
                MoveValidator mv = MoveValidatorFactory.getValidator(check, board, gameNotation);
                if (mv.isValid()) {
                    toggleField(checkPosition);
                }
            }
        }
    }

    public void fillPromotionBoard(BoardPromotion board) {
        for (int row = 0; row < 5; row++) {
          //  promotionPanel.changeButtonText(row, UnicodeChess.pieceToUnicode(board.getPiece(row)));
            promotionPanel.changeButtonIcon(row,board.getPiece(row).pieceColor,board.getPiece(row).pieceType);
        }
    }

    public void winnerText(String text){
        endGamePanel.setWinnerText(text);
    }

    public void displayBoard(Board board) {
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                Position p = new Position(row, col);
                /* display chess pieces as unicode text
                 boardPanel.changeButtonText(p, UnicodeChess.pieceToUnicode(board.getPiece(p)));
                */
                boardPanel.changeButtonIcon(p, board);

            }
        }
    }
}